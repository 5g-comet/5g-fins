# -----------------------------------------------------------
# contains domain-specific scientific calculations as well as randomized data generation to populate the factory
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

import math
import abc
import numpy as np
import psutil
import fgfins as fn
import fgfins.linalgebrahelpers as fnlin
import fgfins.view.matplotview as mpv
import scipy.constants as const
from typing import TypedDict, NamedTuple, Union, List, Optional
import os
import warnings
from PyQt6 import QtCore
from tqdm import tqdm
import time
import multiprocessing as mp


class PLparamProfile(NamedTuple):
    """
    each paramProfile contains information about one parameter of the pathloss model:
    default, min and max values for the parameter, a descriptive label and a "displayfactor".
    This is a number by which the parameter is divided before display in the GUI.
    e.g. in the data structure, frequency is 3e9 Hz, but in the GUI it is displayed as 3 GHz, so the
    displayfactor is 1e9.
    """
    default: float
    min: float
    max: float
    displayfactor: float
    label: str


class PLParameters(TypedDict, total=False):
    """
    The parameters in the PLModels are not, as usual, stored in single object variables. Instead, a dict is used.
    This way, we can count through all Parameters in the GUI for display.
    Contains all possible parameters in all PLModels. When implementing a new PLmodel, add its parameters here.
    Type is either PLparamProfile (for class variables; containing the profiles for each parameter of the class) or
    float (for PLModel objects; containing the values currently set for the parameter in the PLModel.
    """
    freq: Union[PLparamProfile, float]
    alpha: Union[PLparamProfile, float]
    beta: Union[PLparamProfile, float]
    gamma: Union[PLparamProfile, float]
    SF_std: Union[PLparamProfile, float]
    n_nlos: Union[PLparamProfile, float]
    n_los: Union[PLparamProfile, float]
    SF_std_nlos: Union[PLparamProfile, float]
    SF_std_los: Union[PLparamProfile, float]
    d_clutter: Union[PLparamProfile, float]
    h_c: Union[PLparamProfile, float]
    r: Union[PLparamProfile, float]


class PLModel:
    """
    Abstract class combining the common functionality of all path loss models.
    Its purpose is to provide a generic interface to functions that should work with any.
    Look at this code to find comments for code lines which are the same in all PLModels
    """

    # set this in every inherited PLModel
    paramProfiles = PLParameters()  # contains the parameters for all parameters used in this class

    @property
    @abc.abstractmethod
    def paramSet(self):
        """
        contains the currently set values for all parameters used in this class
        """
        return PLParameters()

    @property
    @abc.abstractmethod
    def ui_title(self):
        """
        Descriptive name of the path loss model. It is used by the GUI.
        """
        return ""

    @property
    @abc.abstractmethod
    def ui_documentation(self):
        """
        A string in HTML (rich text) format that describes the path loss model and its parameters.
        It is used by the GUI to explain the path loss model to the user, including its limits and use cases.
        """
        return ""

    @abc.abstractmethod
    def calculate_pathloss(self, distance: float, rng: np.random.Generator, *args, **kwargs) -> float:
        """
        The pathloss calculation is different for each PL model.
        Hence, this abstract class only contains the constraint that its subclasses must provide a function for that.
        :param distance: distance between RD and UE in [m]
        :param rng: Numpy random number generator to draw random numbers from
        """
        pass

    def __getstate__(self):
        return [type(self), self._paramSet]

    def __setstate__(self, state):
        self._paramSet = state[1]

    # noinspection PyTypedDict
    def _set_param(self, paramkey: str, value: float) -> None:
        """
        Tries to set the value of the parameter given my *paramkey* to *value*. If *value* is outside the permitted
        range of value for this parameter, returns a ValueError with a significant error message.
        :param paramkey: key of parameter to set
        :param value: value to set the parameter to
        """
        if paramkey in self.paramProfiles:
            # check range
            if self.paramProfiles[paramkey].min <= value <= self.paramProfiles[paramkey].max:
                self.paramSet[paramkey] = value
            else:
                paramprofile = self.paramProfiles[paramkey]

                raise ValueError(paramprofile.label + ':' + os.linesep + ' ' * 5 + 'Your value is '
                                 + ('too low' if value < paramprofile.min else 'too high')
                                 + '. Valid input range: [' + str(paramprofile.min / paramprofile.displayfactor)
                                 + "–" + str(paramprofile.max / paramprofile.displayfactor) + "].")
        else:
            raise KeyError("Parameter not in PLModel")

    def _set_params(self, params: list[tuple[str, float]]):
        """
        Tries to set a list of parameters to the given values. If any value is out of the permitted range of its
        associated parameter, returns a value error with a significant error message for every faulty parameter value.
        :param params: list of tuples. Each tuple contains one parameter (as parameter key string) and a value (float)
        to set the parameter to.
        """
        error_list = []
        for param in params:
            try:
                self._set_param(*param)
            except ValueError as err:
                error_list.append(str(err))

        if error_list:
            # error messages are concatenated and separated by a blank line, then forwarded collectively
            raise ValueError((2 * os.linesep).join(error_list))

    def wall_induced_path_loss(self, walls: List[fn.Wall] = None) -> float:
        """
        Calculate the path loss (in dB) which is added to a signal loss when it passes through a set of walls.
        :param walls: List of walls on the line of sight between a Radio Dot and a Point of Interest. The signal has
        to pass through the walls, hence the path loss increases.
        :return:
        """
        pl = 0  # start with zero, add for each encountered wall
        for wall in walls:
            # there are walls on the line of sight
            if wall.material == fn.WallMaterial.CONCRETE:
                a = 5.24
                b = 0.0
                c = 0.0462
                d = 0.7822
            elif wall.material == fn.WallMaterial.BRICK:
                a = 3.91
                b = 0.0
                c = 0.0238
                d = 0.16
            elif wall.material == fn.WallMaterial.GLASS:
                a = 6.31
                b = 0.0
                c = 0.0036
                d = 1.3394
            else:
                # if this occurs, you probably forgot to add a wall type in this list
                raise TypeError("Wall material unknown to the path loss model.")

            eta = a * (self.paramSet['freq'] / 1e9) ** b
            theta = c * (self.paramSet['freq'] / 1e9) ** d

            # attenuation rate [dB/m]
            att = 1636 * theta / np.sqrt(eta)

            pl += att * wall.thickness

        return pl


class FSPLModel(PLModel):
    """
    Class providing functionality of the free-space (FS) path loss model. It's only instantiated once.

    Reference:
    [1] Obeidat, Huthaifa & Asif, R. & Obeidat, Omar & Ali, Nazar & Jones, S.
    & Shuaieb, Wafa & Al-Sadoon, Mohammed & Hameed, K. & Alabdullah,
    Ali & Dama, Yousef & Abd-Alhameed, Raed. (2018). An Indoor Path Loss
    Prediction Model using Wall Correction Factors for WLAN and
    5G Indoor Networks. Radio Science. 53. 10.1002/2018rs006536.
    [2] ITU-R P.2040-2


    Implementation of Effective Wall Loss Model [1] for materials: Concrete, brick and glass [2]
    """

    paramProfiles = PLParameters()
    paramProfiles['freq'] = PLparamProfile(3.7e9, 1e9, 3e10, 1e9, "frequency [GHz]")

    ui_title = "FSPL (Free space path loss)"
    ui_documentation = """
        <p>The <em>free-space path loss model</em> describes the attenuation of energy between the radio dot and the
        user that occurs in a line-of-sight path through free-space (losses in air).</p>
        <ul>
        <li>
        <p style="margin-bottom: 0.28cm; line-height: 108%;">Frequency restricted to accommodate 5G mid-bands and
        millimeter wave bands <strong>(Adjustable)</strong></p>
        </li>
        </ul>
        """

    def __init__(self, frequency=paramProfiles['freq'].default):
        """
        :param frequency: frequency of the PL model in [Hz]
        """
        self.paramSet = PLParameters()
        self._set_param('freq', frequency)
        self.pl_at_1m_distance = self.calculate_pathloss(1)

    def calculate_pathloss(self, distance: float, rng=np.random.default_rng(), walls: List[fn.Wall] = None, *args,
                           **kwargs) -> float:
        """
        Calculate the path loss given the distance and the frequency which is set at the initialization of the PL model,
        as well as a list of the walls which cross the direct path between RD and point of interest.
        Limitation: The minimum returned value of this function is the path loss at a distance of 1 meter.
        :param distance: distance between the currently calculated UE / pixel and the associated RD in [m]
        :param rng: Numpy random number generator to draw random numbers from
        :param walls: list of walls which influence the path loss
        :return: max(path loss in [dB] at 1 meter distance, path loss in [dB] at *distance*) + attenuation in [dB]
        through wall
        """
        if walls is None:
            walls = []

        # calculate actual path loss at given distance
        pl_at_distance = 20 * math.log10((4 * math.pi * self.paramSet['freq'] * distance) / const.speed_of_light)

        # to allow calculation of pl at 1 meter distance during initialization
        if distance == 1:
            return pl_at_distance

        # minimum returned path loss: at 1 meter distance
        pl = max(self.pl_at_1m_distance, pl_at_distance)

        # if there is a wall influencing the path loss, the path loss is increased by the path loss occurring in wall
        pl = pl + self.wall_induced_path_loss(walls)

        return pl

    @property
    def paramSet(self):
        return self._paramSet

    @paramSet.setter
    def paramSet(self, value):
        self._paramSet = value

    def __getstate__(self):
        return [type(self), self._paramSet, self.pl_at_1m_distance]

    def __setstate__(self, state):
        self._paramSet = state[1]
        self.pl_at_1m_distance = state[2]


class ABGPLModel(PLModel):
    """
    Class providing functionality of the ABG path loss model. It's only instantiated once.

    References:
    [1] K. Haneda et al., “Indoor 5G 3GPP-like channel models for office
    and shopping mall environments,” in IEEE International Conference on
    Communications Workshops (ICC), May 2016, pp. 694–699.
    [2] Obeidat, Huthaifa & Asif, R. & Obeidat, Omar & Ali, Nazar & Jones, S.
    & Shuaieb, Wafa & Al-Sadoon, Mohammed & Hameed, K. & Alabdullah,
    Ali & Dama, Yousef & Abd-Alhameed, Raed. (2018). An Indoor Path Loss
    Prediction Model using Wall Correction Factors for WLAN and
    5G Indoor Networks. Radio Science. 53. 10.1002/2018rs006536.
    [3] ITU-R P.2040-2

    Constants are determined through optimization to minimize the SF standard deviation over a measured PL data set [1]
    Implementation of Effective Wall Loss Model [2] for materials: Concrete, brick and glass [3]
    """
    paramProfiles = PLParameters()
    paramProfiles['freq'] = PLparamProfile(3.7e9, 1e9, 3e10, 1e9, "frequency [GHz]")
    paramProfiles['alpha'] = PLparamProfile(3.83, 0, 7, 1, "alpha")
    paramProfiles['beta'] = PLparamProfile(17.30, 0, 40, 1, "beta")
    paramProfiles['gamma'] = PLparamProfile(2.49, 0, 5, 1, "gamma")
    paramProfiles['SF_std'] = PLparamProfile(8.03, 1, 15, 1, "shadow fading standard deviation")

    ui_title = "ABG (Alpha Beta Gamma single slope)"
    ui_documentation = """
        <p>In the <em>alpha-beta-gamma path loss model</em> three parameters/constants are determined through
        optimization to minimize the SF standard deviation over a measured PL data set. Alpha indicates the effect of
        distance on path loss, beta is an optimization parameter and gamma indicates the effect of frequency on path
        loss.</p>
        <ul>
        <li>
        <p>Frequency restricted to accommodate 5G mid-bands and millimeter wave bands <strong>(Adjustable)</strong></p>
        </li>
        <li>
        <p>Alpha, beta, gamma and sf_std restricted by parameters given in: <a
        href="http://www.5gworkshops.com/2016/5G_Channel_Model_for_bands_up_to100_GHz(2015-12-6).pdf">
        http://www.5gworkshops.com/2016/5G_Channel_Model_for_bands_up_to100_GHz(2015-12-6).pdf</a></p>
        </li>
        </ul>
        """

    def __init__(self, frequency=paramProfiles['freq'].default, alpha=paramProfiles['alpha'].default,
                 beta=paramProfiles['beta'].default,
                 gamma=paramProfiles['gamma'].default,
                 SF_std=paramProfiles['SF_std'].default):
        """
        :param frequency: frequency of the PL model in [Hz]
        :param alpha: alpha constant (as given in the reference sheet for InH-Indoor-Office-NLOS single slope (FFS))
        Constant coefficient which indicates effect of distance on path loss
        :param beta: beta constant (as given in the reference sheet for InH-Indoor-Office-NLOS single slope (FFS))
        Offset value / optimization parameter
        :param gamma: gamma constant (as given in the reference sheet for InH-Indoor-Office-NLOS single slope (FFS))
        Constant coefficient which indicates effect of frequency on path loss
        :param SF_std: Standard deviation of shadow fading term
        """
        self.paramSet = PLParameters()
        self._set_params([('freq', frequency), ('alpha', alpha), ('beta', beta), ('gamma', gamma), ('SF_std', SF_std)])

    def calculate_pathloss(self, distance: float, rng=np.random.default_rng(), walls: List[fn.Wall] = None, *args,
                           **kwargs) -> float:
        """
        Calculate the pathloss given the distance and the frequency which is set at the initialization of the PL model
        :param distance: distance between the currently calculated UE / pixel and the associated RD in [m]
        :param rng: Numpy random number generator to draw random numbers from
        :param walls: list of walls which influence the path loss
        :return: path loss in [dB]
        """
        freq_ghz = self.paramSet['freq'] / 1e9  # convert the Hz frequency to GHz for use in the equation
        if walls is None:
            walls = []

        #  return PL_ABG(frequency, distance) as given in the path loss reference sheet
        pl = 10 * self.paramSet['alpha'] * math.log10(distance) \
             + self.paramSet['beta'] \
             + 10 * self.paramSet['gamma'] * math.log10(freq_ghz) \
             + rng.normal(0, self.paramSet['SF_std'])

        # if there is a wall influencing the path loss, the path loss is increased by the path loss occurring in wall
        pl = pl + self.wall_induced_path_loss(walls)

        return pl

    @property
    def paramSet(self):
        return self._paramSet

    @paramSet.setter
    def paramSet(self, value):
        self._paramSet = value

    def __getstate__(self):
        return [type(self), self._paramSet]

    def __setstate__(self, state):
        self._paramSet = state[1]


class CIPLModel(PLModel):
    """
    Class providing functionality of the close-in (CI) path loss model

    References:
    [1] K. Haneda et al., “Indoor 5G 3GPP-like channel models for office
    and shopping mall environments,” in IEEE International Conference on
    Communications Workshops (ICC), May 2016, pp. 694–699.
    [2] Obeidat, Huthaifa & Asif, R. & Obeidat, Omar & Ali, Nazar & Jones, S.
    & Shuaieb, Wafa & Al-Sadoon, Mohammed & Hameed, K. & Alabdullah,
    Ali & Dama, Yousef & Abd-Alhameed, Raed. (2018). An Indoor Path Loss
    Prediction Model using Wall Correction Factors for WLAN and
    5G Indoor Networks. Radio Science. 53. 10.1002/2018rs006536.
    [3] ITU-R P.2040-2

    Constants are determined through optimization to minimize the SF standard deviation over a measured PL data set [1]
    Implementation of Effective Wall Loss Model [2] for materials: Concrete, brick and glass [3]
    """

    # Constants for InH-Indoor-Office LOS & NLOS single slope (FFS) are given in Table 2 of reference
    paramProfiles = PLParameters()
    paramProfiles['freq'] = PLparamProfile(3.7e9, 1e9, 3e10, 1e9, "frequency [GHz]")
    paramProfiles['n_nlos'] = PLparamProfile(3.19, 1, 5, 1, "path loss exponent NLOS")
    paramProfiles['n_los'] = PLparamProfile(1.73, 1, 5, 1, "path loss exponent LOS")
    paramProfiles['SF_std_nlos'] = PLparamProfile(8.29, 1, 15, 1, "shadow fading standard deviation NLOS")
    paramProfiles['SF_std_los'] = PLparamProfile(3.02, 1, 15, 1, "shadow fading standard deviation LOS")

    ui_title = "CI (Close-in free space single slope)"
    ui_documentation = """
        <p>In the <em>close-in (single slope) path loss model</em> a 1 m free-space close-in reference distance path
        loss anchor is used. A distance-dependent path loss component is added with a path loss exponent that is
        determined through optimization to minimize the SF standard deviation over a measured PL data set. Finally,
        the shadow fading standard deviation is added to the path loss.</p>
        <ul>
        <li>
        <p>Frequency restricted to accommodate 5G mid-bands and millimeter wave bands <strong>(Adjustable)</strong></p>
        </li>
        <li>
        <p>n (los, nlos) and SF_std (los, nlos) restricted by parameters given in: <a
        href="http://www.5gworkshops.com/2016/5G_Channel_Model_for_bands_up_to100_GHz(2015-12-6).pdf">
        http://www.5gworkshops.com/2016/5G_Channel_Model_for_bands_up_to100_GHz(2015-12-6).pdf</a></p>
        </li>
        </ul>
        """

    def __init__(self, frequency=paramProfiles['freq'].default,
                 n_nlos=paramProfiles['n_nlos'].default,
                 n_los=paramProfiles['n_los'].default,
                 SF_std_nlos=paramProfiles['SF_std_nlos'].default,
                 SF_std_los=paramProfiles['SF_std_los'].default):
        """
        :param frequency: frequency of the PL model in [Hz]
        :param n_nlos: path loss exponent, non line of sight (as given in the reference sheet)
        :param n_los: path loss exponent, line of sight (as given in the reference sheet)
        :param SF_std_nlos: shadow fading standard deviation, non line of sight (as given in the reference sheet)
        :param SF_std_los: shadow fading standard deviation, line of sight (as given in the reference sheet)
        """
        self.paramSet = PLParameters()
        self._set_params([('freq', frequency), ('n_nlos', n_nlos), ('n_los', n_los), ('SF_std_nlos', SF_std_nlos),
                          ('SF_std_los', SF_std_los)])
        self.fspl = FSPLModel(self.paramSet['freq'])  # uses an FSPL model internally, which is created here

    def calculate_pathloss(self, distance: float, rng=np.random.default_rng(), walls: List[fn.Wall] = None, *args,
                           **kwargs) -> float:
        """
        Calculate the pathloss given the distance and the frequency which is set at the initialization of the PL model
        :param distance: distance between the currently calculated UE / pixel and the associated RD in [m]
        :param rng: Numpy random number generator to draw random numbers from
        :param walls: list of walls which influence the path loss
        :param material: string containing selected material for wall (concrete, brick, glass)
        :param wall_thickness: typical indoor wall thickness in m
        :return: path loss in [dB]
        """
        if walls is None:
            walls = []

        # Given the probability distribution, calculate whether the current pixel / UE should be calculated as
        # line-of-sight or fn-line-of-sight
        is_los = int(rng.uniform() > self.get_plos(distance))  # 1 == LOS, 0 == NLOS

        #  return PL_CI(frequency, distance) as given in the path loss reference sheet
        if is_los == 1:
            pl = self.fspl.calculate_pathloss(distance) \
                 + 10 * self.paramSet['n_los'] * math.log10(distance) \
                 + rng.normal(0, self.paramSet['SF_std_los'])  # normal distribution given standard deviation SF_std
        else:
            pl = self.fspl.calculate_pathloss(distance) \
                 + 10 * self.paramSet['n_nlos'] * math.log10(distance) \
                 + rng.normal(0, self.paramSet['SF_std_nlos'])  # normal distribution given standard deviation SF_std

        # if there is a wall influencing the path loss, the path loss is increased by the path loss occurring in wall
        pl = pl + self.wall_induced_path_loss(walls)

        return pl

    @staticmethod
    def get_plos(distance: float):
        """
        Determine the probability that a pixel/UE at a given distance should be treated as in line-of-sight (otherwise
        non-line-of-sight)
        :param distance: distance between the currently calculated UE / pixel and the associated RD in [m]
        :return: probability of LOS
        """

        # Formula for probability of LOS in InH environment is given in Table 1 of reference
        if distance <= 1.2:
            return 1
        elif distance < 6.5:
            return math.exp(-(distance - 1.2) / 4.7)
        else:
            return math.exp(-(distance - 6.5) / 32.6) * 0.32

    @property
    def paramSet(self):
        return self._paramSet

    @paramSet.setter
    def paramSet(self, value):
        self._paramSet = value

    def __getstate__(self):
        return [type(self), self._paramSet, self.fspl]

    def __setstate__(self, state):
        self._paramSet = state[1]
        self.fspl = state[2]


class InFSHPLModel(PLModel):
    """
    Class providing functionality of the Indoor Factory with sparse clutter and high BS (InF-SH) path loss model

    References:
    [1] ETSI TR 138 901 V16.1.0
    [2] Jesper Ahlander, Maria Posluk. "Deployment Strategies for High Accuracy and
    Availability Indoor Positioning with 5G". MSc Thesis. Linköping University, 2020.
    [3] Obeidat, Huthaifa & Asif, R. & Obeidat, Omar & Ali, Nazar & Jones, S.
    & Shuaieb, Wafa & Al-Sadoon, Mohammed & Hameed, K. & Alabdullah,
    Ali & Dama, Yousef & Abd-Alhameed, Raed. (2018). An Indoor Path Loss
    Prediction Model using Wall Correction Factors for WLAN and
    5G Indoor Networks. Radio Science. 53. 10.1002/2018rs006536.
    [4] ITU-R P.2040-2

    Implementation of Effective Wall Loss Model [3] for materials: Concrete, brick and glass [4]
    """

    # Constants are given in Table 7.2-4 of [1]
    # Clutter here refers to objects on the factory floor, such as machines, assembly lines, storage racks, etc.

    paramProfiles = PLParameters()
    paramProfiles['freq'] = PLparamProfile(3.7e9, 1e9, 3e10, 1e9, "frequency [GHz]")

    # Clutter size defined in Table 7.2-4 of [1]
    paramProfiles['d_clutter'] = PLparamProfile(10, 1, 20, 1, "clutter size")

    # Effective clutter height must be 0 < h_c < ceiling height = 10m
    paramProfiles['h_c'] = PLparamProfile(3.111, 1, 10, 1, "effective clutter height")
    paramProfiles['r'] = PLparamProfile(0.3, 0.1, 0.3999, 1, "clutter density")  # Clutter density < 40%

    # Standard deviation of shadow fading term given in Table 7.4.1-1 of [1]
    paramProfiles['SF_std'] = PLparamProfile(5.9, 1, 10, 1, "shadow fading standard deviation")

    ui_title = "InF-SH (Indoor Factory with sparse clutter and high BS)"
    ui_documentation = """
        <p>In the <em>Indoor Factory with Sparse clutter and High base station height path loss model</em>, path loss
        formulas from the ETSI/3GPP Technical Report are implemented. Additionally, a line-of-sight probability is
        introduced that is affected by adjustable parameters describing the factory floor.</p>
        <ul>
        <li>
        <p>Frequency restricted to accommodate 5G mid-bands and millimeter wave bands <strong>(Adjustable)</strong></p>
        </li>
        <li>
        <p>d_clutter, h_c, r restricted by ETSI TR 138 901 V16.1.0 <strong>(Adjustable)</strong></p>
        </li>
        <li>
        <p>SF_std restricted by ETSI TR 138 901 V16.1.0</p>
        </li>
        </ul>
        """

    def __init__(self, frequency=paramProfiles['freq'].default,
                 d_clutter=paramProfiles['d_clutter'].default,
                 h_c=paramProfiles['h_c'].default,
                 r=paramProfiles['r'].default,
                 SF_std=paramProfiles['SF_std'].default):
        """
        :param frequency: frequency of the PL model in [Hz]
        :param d_clutter: clutter size (as given in reference sheet, Table 7.2-4)
        :param h_c: Effective clutter height (must be smaller than factory height)
        :param r: clutter density
        :param SF_std: shadow fading standard deviation (as given in reference sheet, Table 7.4.1-1)
        """
        self.paramSet = PLParameters()
        self._set_params([('freq', frequency), ('d_clutter', d_clutter), ('h_c', h_c), ('r', r), ('SF_std', SF_std)])

    def calculate_pathloss(self, distance: float, rng=np.random.default_rng(), rd_zpos=8, zpos=1.5,
                           walls: List[fn.Wall] = None, *args, **kwargs) \
            -> float:
        """
        Calculate the pathloss given the distance and the frequency which is set at the initialization of the PL model
        :param distance: distance between the currently calculated UE / pixel and the associated RD in [m]
        :param rng: Numpy random number generator to draw random numbers from
        :param rd_zpos: height of radiodot in [m]
        :param zpos: height of user in [m]
        :param walls: list of walls which influence the path loss
        :return: path loss in [dB]
        """
        if walls is None:
            walls = []

        freq_ghz = self.paramSet['freq'] / 1e9  # Normalization by 1GHz

        h_ut = zpos  # User height. Typically 1.5m based on [2]
        h_bs = rd_zpos  # Radiodot height > user height. Typically 8m based on [2]

        """
        # User height must be unequal to clutter height to avoid division-by-zero error
        # Model definition requires radiodot height to be > than user height
        """

        if h_bs <= h_ut or self.paramSet['h_c'] == h_ut:
            return np.nan

        # Constant defined in Table 7.4.2-1 of [1]
        k_subsce = -(self.paramSet['d_clutter'] * (h_bs - h_ut)) / \
                   (np.log(1 - self.paramSet['r']) * (self.paramSet['h_c'] - h_ut))

        # Formulas given in Table 7.4.1-1 (InF-SH) of [1]
        pl_inf_sh = 32.4 + 23 * np.log10(distance) + 20 * np.log10(freq_ghz)
        pl_los = 31.84 + 21.5 * np.log10(distance) + 19 * np.log10(freq_ghz)
        prob_rand = abs(rng.normal(0, self.paramSet['SF_std'])) / self.paramSet['SF_std']
        prob_los = math.exp(-distance / k_subsce)

        if prob_rand < prob_los:
            pl = pl_los
        else:
            pl = max(pl_los, pl_inf_sh)

        # if there is a wall influencing the path loss, the path loss is increased by the path loss occurring in wall
        pl = pl + self.wall_induced_path_loss(walls)

        return pl

    @property
    def paramSet(self):
        return self._paramSet

    @paramSet.setter
    def paramSet(self, value):
        self._paramSet = value

    def __getstate__(self):
        return [type(self), self._paramSet]

    def __setstate__(self, state):
        self._paramSet = state[1]


def calculate_pathloss_matrix(plmodel: PLModel, rd_list: list[fn.RadioDot], x: float, y: float,
                              zpos: float, wall_list: list[fn.Wall] = None,
                              worker: Optional[mpv.Worker] = None) -> np.ndarray:
    """
    Given the size of the factory, the RDs, the walls and the chosen path-loss model, calculate the path-loss for each
    pixel (== each meter: 1 pixel == 1 meter) of the factory. This is called to calculate the heatmap values.
    :param plmodel: path-loss model - including frequency and all constants
    :param rd_list: list of Radio Dots with their positions
    :param x: x-size of the factory in [meters]
    :param y: y-size of the factory in [meters]
    :param zpos: height in [meters] for which the path-loss should be calculated
    :param wall_list: list of walls
    :param worker: If used in GUI mode, "worker" is a wrapper class to this function executed on a separate thread to
    ensure proper responsiveness of the GUI and visual updates to the progress bar. For interaction, the wrapping worker
    object is a parameter to this function. If worker is not given, this function reports progress to the command line.
    :return: Numpy 2D-array with the path-loss values in [dB]
    """
    if wall_list is None:
        wall_list = []

    # if fn RDs given, don't calculate - as it doesn't make sense.
    if not rd_list:
        return np.zeros(shape=(0, 0))

    # if x (and y) is not integer, we have to calculate the pathloss for the range between floor(x) and x too. (resp. y)
    x_up = math.ceil(x)
    y_up = math.ceil(y)

    progress_max = x_up * y_up
    cli_progressbar = None

    if not worker:
        # tqdm package provides CLI-based progress bar
        cli_progressbar = tqdm(total=progress_max)

    result = []  # results are appended to this list in the multiprocessing loop

    time_start = time.time()

    # collect the arguments for the distribution of determine_pathloss_packed to the cpus
    x_list = [i // y_up for i in range(0, x_up * y_up)]
    y_list = [i % y_up for i in range(0, x_up * y_up)]
    argumentlist = list(zip([plmodel] * x_up * y_up, [rd_list] * x_up * y_up, x_list, y_list, [zpos] * x_up * y_up,
                            [wall_list] * x_up * y_up))

    # determine number of physical cores using the psutil library
    num_cpus = psutil.cpu_count(logical=False)
    # why get_context("spawn")? -> https://pythonspeed.com/articles/python-multiprocessing/
    with mp.get_context("spawn").Pool(processes=num_cpus) as workpool:
        # too small chunk sizes take far too much time, but overly long chunk sizes increase the preparation time
        # before the distributed calculation actually begins. So the tradeoff is the largest of the factory dimensions.
        for i, pl in enumerate(workpool.imap(determine_pathloss_packed, argumentlist, chunksize=max(x_up, y_up))):
            result.append(pl)
            if worker:
                # GUI-mode
                with QtCore.QMutexLocker(worker.mutex) as mutex:
                    if worker.stopped:
                        # if calculation stop was requested by user (by pressing the "Abort" button)
                        return np.zeros(shape=(0, 0))
                    elif time.time() - time_start > 0.2:  # or progress_max
                        # update the progress bar. Only if 0.2 seconds have passed since the last update.
                        worker.progress.emit(i)
                        time_start = time.time()
                        mutex.unlock()
                        time.sleep(0.1)  # allow for switch of threads to update progress bar and handle inputs

            else:
                # cli mode
                cli_progressbar.update(1)

        if worker:
            with QtCore.QMutexLocker(worker.mutex):
                # The last progress bar update (100 % progress) must be pushed to indicate 100 % and close the gui
                # progress bar.
                worker.progress.emit(x_up * y_up)

        return np.reshape(result, (x_up, y_up))


def calculate_power_matrix(pl_matrix: np.ndarray, power: float) -> np.ndarray:
    """
    Transforms a path loss values matrix into a power values matrix.
    @param pl_matrix: pathloss matrix
    @param power: The transmitted power of a radio dot. Aassumed equal for all radio dots. [dBm]
    @return: power matrix
    """
    x, y = pl_matrix.shape

    power_m = np.ones((x, y)) * power

    return np.subtract(power_m, pl_matrix)


def calculate_snr_matrix(pl_matrix: np.ndarray, power: float, noise: float) -> np.ndarray:
    """
    Transforms a path loss values matrix into a SNR values matrix.
    @param pl_matrix: pathloss matrix
    @param power: The transmitted power of a radio dot. Aassumed equal for all radio dots. [dBm]
    @param noise: The assumed environmental signal noise [dBm]
    @return: snr matrix
    """
    x, y = pl_matrix.shape

    power_m = calculate_power_matrix(pl_matrix, power)
    noise_m = np.ones((x, y)) * noise

    return np.subtract(power_m, noise_m)


def determine_pathloss_packed(pack: tuple[PLModel, list[fn.RadioDot], float, float, float, list[fn.Wall]]) -> float:
    """
    This function calls determine_pathloss with all of its arguments in one tuple. We need this to pass
    determine_pathloss to the multiprocessing cpu pool.
    :param pack: arguments
    :return:
    """
    return determine_pathloss(*pack)


def determine_pathloss(plmodel: PLModel, rd_list: list[fn.RadioDot], xpos: float, ypos: float, zpos: float,
                       wall_list: list[fn.Wall] = None) -> float:
    """
    Given the path-loss model, the RDs (with their positions), the walls and the coordinates of a point of interest
    (UE/pixel), calculate the path loss for this point using the path-loss model's path loss calculation function.
    This function calculates the distances between the point of interest and each RD from the list and calls the PL's
    calculation function with those. The lowest path loss wins.
    :param plmodel: path-loss model - including frequency and all constants
    :param rd_list: list of Radio Dots with their positions
    :param xpos: x-coordinate of the point of interest in [meters]
    :param ypos: y-coordinate of the point of interest in [meters]
    :param zpos: z-coordinate of the point of interest in [meters]
    :param wall_list: list of walls
    :return: pathloss in [db]
    """
    if wall_list is None:
        wall_list = []

    # create new random number generator for the process
    rng = np.random.default_rng()

    # list of distances between RDs and POI (point of interest)
    distances = [np.linalg.norm((rd.xpos - xpos, rd.ypos - ypos, rd.zpos - zpos)) for rd in rd_list]

    # list of list of walls between RDs and POI (as each line of sight can have several walls blocking its direct way)
    walls_per_rd_los = list()
    for rd in rd_list:
        # for each RD, a list of the walls blocking the line of sight between the RD and the POI is generated
        walls_on_los = list()
        for wall in wall_list:
            # get the intersection point between the wall and the line of sight between RD and POI
            intersect = fnlin.get_intersect(np.array([rd.xpos, rd.ypos, rd.zpos]), np.array([xpos, ypos, zpos]),
                                            np.array([wall.startx, wall.starty, 0]),
                                            np.array([wall.startx, wall.starty, wall.height]),
                                            np.array([wall.endx, wall.endy, 0]))
            if intersect and intersect[1]:
                # there is a singular point of intersection and it is within the wall's area
                walls_on_los.append(wall)
                continue

        walls_per_rd_los.append(walls_on_los)

    rd_zposes = [rd.zpos for rd in rd_list]
    # calculate the path losses from RDs to our POI, taking into account the walls and everything
    pathlosses = [plmodel.calculate_pathloss(distance, rd_zpos=rd_zpos, zpos=zpos, walls=walls_on_this_path, rng=rng)
                  for distance, rd_zpos, walls_on_this_path in zip(distances, rd_zposes, walls_per_rd_los)]

    # the smallest path loss wins i.e. the RD with the best connection is "connected" to any UE at our POI.
    # if fn connection is possible from any RD, return NaN. (In this case, nanmin issues a warning, which we suppress)
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        if not pathlosses:
            return np.nan
        else:
            return float(np.nanmin(pathlosses))
