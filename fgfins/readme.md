# 5G-FINS

## Structure of the fgfins package
This package contains all code relevant to the software. It is partitioned as follows:

- `views/` - code regarding the layout and functionality of the main window and everything inside. The main menu and toolbar. The plotting environment and the code that determines how data is visualized. The sidebar views and layer control box.
- `__init__.py` - contains important classes and definitions used throughout the whole software: `Factory`, `Actor`, `RadioDot`, `UserEntity` as well as some important enums. The software version string is also defined here. Executed automatically when the package is loaded.
- `datagenerator.py` - everything related to the pathloss models and domain-related calculations. Also contains code to create a set of random RadioDots and UserEntities. When coding a CLI-based Python script that should calculate some numbers, the classes and objects in here are your friends. (And the classes in `__init__.py` of course)
- `model.py` - this code defines how the data is stored and accessed during the execution of the app
- `persistancehandler.py` - handles the storage or restoring of the model data to / from a file on hard disk.
