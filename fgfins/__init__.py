# -----------------------------------------------------------
# contains definitions for domain-specific classes and enums used throughout the whole software
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

import abc
from enum import IntEnum

__version__ = '0.32'
__compatible_ver__ = '0.31'  # minimal software version for file compatibility


class Factory:
    """
    contains all the properties of a factory hall.
    """
    def __init__(self, x: int, y: int, z: int):
        self.x = x  # set dimensions (size) of factory
        self.y = y
        self.z = z

    def __getstate__(self):
        """
        serialization function to allow transforming an instance of the class to a pickle
        """
        return [self.x, self.y, self.z]

    def __setstate__(self, state):
        """
        deserialization function to allow transforming an instance of the class to a pickle
        """
        self.x, self.y, self.z = state


class Item(abc.ABC):
    @abc.abstractmethod
    def __init__(self):
        pass


class Actor(Item):
    """
    Abstract class combining the common functionality of RadioDot and UserEndpoint.
    Its purpose is to provide a generic interface to functions that should work with both.
    """

    @abc.abstractmethod
    def __init__(self, xpos: float, ypos: float, zpos: float):
        self.xpos, self.ypos, self.zpos = xpos, ypos, zpos  # position of the actor in the factory
        self.id = 0  # unique RadioDot / UserEndpoint id. Not the same as the python-intern object id!

    def getId(self) -> int:
        return self.id

    @property
    @abc.abstractmethod
    def _idGnr(self):
        # ids are unique only per actor class. Meaning there can be a #1 RD and a #1 UE at the same time.
        # Hence, this abstract class only contains the constraint that its subclasses must provide an ID generator.
        pass

    @abc.abstractmethod
    def __getstate__(self):
        """
        serialization function to allow transforming an instance of the class to a pickle
        """
        pass

    @abc.abstractmethod
    def __setstate__(self, state):
        """
        serialization function to allow transforming an instance of the class to a pickle
        """
        pass


class RadioDot(Actor):
    _idGnr = 1  # id generator, incremented with each generated Radio Dot

    def __init__(self, xpos: float, ypos: float, zpos: float):
        super(RadioDot, self).__init__(xpos, ypos, zpos)  # position of the RD in the factory
        self.id = RadioDot._idGnr
        RadioDot._idGnr = RadioDot._idGnr + 1  # incrementing the id generator

    def __getstate__(self):
        """
        serialization function to allow transforming an instance of the class to a pickle
        """
        return [type(self), [self.xpos, self.ypos, self.zpos, self.id]]

    def __setstate__(self, state):
        """
        deserialization function to allow transforming an instance of the class to a pickle
        """
        self.xpos, self.ypos, self.zpos, self.id = state[1]


class UserEndpoint(Actor):
    _idGnr = 1  # id generator, incremented with each generated User Endpoint

    def __init__(self, xpos: float, ypos: float, zpos: float, req_pl: float):
        super(UserEndpoint, self).__init__(xpos, ypos, zpos)  # position of the RD in the factory
        self.reqPathloss = req_pl  # path loss requirement for this UE
        self.achvPathloss = 0
        self.id = UserEndpoint._idGnr
        UserEndpoint._idGnr = UserEndpoint._idGnr + 1  # incrementing the id generator

    def __getstate__(self):
        """
        serialization function to allow transforming an instance of the class to a pickle
        """
        return [type(self), [self.xpos, self.ypos, self.zpos, self.id, self.reqPathloss, self.achvPathloss]]

    def __setstate__(self, state):
        """
        deserialization function to allow transforming an instance of the class to a pickle
        """
        self.xpos, self.ypos, self.zpos, self.id, self.reqPathloss, self.achvPathloss = state[1]


class WallMaterial(IntEnum):
    """
    Enum for wall materials
    """
    CONCRETE = 0
    BRICK = 1
    GLASS = 2


class Wall(Item):
    """
    A wall in our simulation model is a straight, infinitely thin object which goes from the start point to the
    end point. It has a certain height and its base is always on the floor. It's made from a material.
    """
    def __init__(self, startx: float, starty: float, endx: float, endy: float, height: float, material: WallMaterial,
                 thickness: float):
        self.startx, self.starty, self.endx, self.endy, self.height, self.material, self.thickness = startx, starty,\
            endx, endy, height, material, thickness

    def __getstate__(self):
        """
        serialization function to allow transforming an instance of the class to a pickle
        """
        return [type(self), [self.startx, self.starty, self.endx, self.endy, self.height, self.material,
                             self.thickness]]

    def __setstate__(self, state):
        """
        deserialization function to allow transforming an instance of the class to a pickle
        """
        self.startx, self.starty, self.endx, self.endy, self.height, self.material, self.thickness = state[1]


class Tool(IntEnum):
    """
    Enum for the selected tool in the view
    """
    MOUSE = 0
    ADD_RD = 1
    ADD_UE = 2
    ADD_WALL = 3
    RM_AC = 4


class Roles(IntEnum):
    """
    Roles for model access. Each view receives the data in a format tailored to their specific needs
    See: https://doc.qt.io/qt-5/qt.html#ItemDataRole-enum
    """
    DeviceRole = 16
    QoSRole = 17
    AddedSingleUE = 18


class Metric:
    def __init__(self, name: str, unit: str):
        self.name, self.unit = name, unit


metrics = {"pl": Metric("path loss", "dB"), "snr": Metric("SNR", "dB"), "pw": Metric("power", "dBm")}
