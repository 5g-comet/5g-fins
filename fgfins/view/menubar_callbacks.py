# -----------------------------------------------------------
# contains the callback functions which are triggered when a menubar entry is selected
# Those methods do not have their own class, they are directly imported into Window in view/__init__.py
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

from PyQt6 import QtGui
from PyQt6 import QtCore
from PyQt6.QtWidgets import *
from PIL import Image
from fgfins import persistencehandler as fgph
import fgfins.datagenerator as fgdg
from typing import Type
from pathlib import Path
import matplotlib.pyplot as mpl_plt
import fgfins as fg
import os
import sys


def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
    """
    Executed when the window is closed, by selecting the "Quit" menu entry or clicking the x-button, or other means.
    Contains the logic for the "save changes?" dialog
    :param self: reference to window
    :param a0: event reference
    """
    if self.changed_since_last_save:  # did the user perform unsaved changes?
        # show dialog window
        choice = QMessageBox.question(self, 'Message',
                                      "Would you like to save your changes?", QMessageBox.StandardButton.No |
                                      QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.Abort,
                                      QMessageBox.StandardButton.Yes)

        if choice == QMessageBox.StandardButton.Yes:
            self.save()
            sys.exit()
        elif choice == QMessageBox.StandardButton.No:
            sys.exit()
        else:  # user aborted
            if a0:
                a0.ignore()  # do not process window close event and do not terminate process
            return
    else:
        sys.exit()  # close and terminate


def open_file(self):
    """
    Called when the user selects "Open file" in the menu. Creates the file selection dialog, reads selected file,
    loads data into the model.
    :param self: reference to window
    """

    #  Filetype of opening dialog should be "Pickle", as pickle is the name of the serialization framework
    filename = QFileDialog.getOpenFileName(self, "Open file", str(Path.home()), "Pickles (*.pickle)")

    if not filename[0]:
        return  # loading aborted

    try:
        newmodel = fgph.load_data(filename[0])  # try to construct a model from the file data
    except ImportError as err:  # file of wrong format or from another version? Show error in a popup window.
        box = QMessageBox()
        box.setWindowTitle("Error!")
        box.setText("Loading didn't work! The following error occurred:")
        box.setInformativeText(str(err))
        box.exec()
        return

    self.model.assign(newmodel)  # override old model with the loaded one
    self.cur_savepath = filename[0]  # the opened file should be the one which is written into when "Save" is clicked

    self.changed_since_last_save = False  # reset the "change since last save" to remove the little "*" in the title bar
    self.updateWindowTitle()  # and update window title to show the new file path without the "*"
    return


def save(self):
    """
    Called when the user clicks selects "Save" in the menu. Tries to save the data into the opened file. If fn file
    has been opened or saved to yet, calls the "save as" dialog.
    :param self: Reference to window
    """
    if not self.cur_savepath:  # fn file has been opened or saved to yet
        self.saveas()
        return

    try:
        fgph.save_data(self.model, self.cur_savepath)
    except Exception as err:  # fn disk space? No permissions? Shows the error in an information popup window.
        box = QMessageBox()
        box.setText("Saving didn't work! The following error occurred:")
        box.setInformativeText(str(err))
        box.exec()
        return

    self.changed_since_last_save = False  # reset the "change since last save" to remove the little "*" in the title bar
    self.updateWindowTitle()  # and update window title to actually remove the "*"


def saveas(self):
    """
    Called when the user clicks selects "Save as" in the menu. Opens the location selection dialog and tries to save
    the data into the selected directory.
    :param self: Reference to window
    :return:
    """
    #  Filetype of opening dialog should be "Pickle", as pickle is the name of the serialization framework
    filename = QFileDialog.getSaveFileName(self, "Save file", str(Path.home().joinpath("myFactory.pickle")),
                                           "Pickles (*.pickle)")
    if not filename[0]:
        return  # saving aborted

    try:
        fgph.save_data(self.model, filename[0])
    except Exception as err:  # fn disk space? No permissions? Shows the error in an information popup window.
        box = QMessageBox()
        box.setWindowTitle("Error!")
        box.setText("Saving didn't work! The following error occurred:")
        box.setInformativeText(str(err))
        box.exec()
        return

    self.cur_savepath = filename[0]  # update path of currently opened file, which is overriden when "Save" is clicked
    self.changed_since_last_save = False  # reset the "change since last save" to remove the little "*" in the title bar
    self.updateWindowTitle()  # and update window title to actually remove the "*"


def export_plot(self):
    """
    Called when the user clicks selects "Export plot" in the menu. Opens the location selection dialog with the right
    image formats and tries to save the data into the selected directory.
    :param self: Reference to window
    """
    dialog = QFileDialog(self)

    # self.filters contains all the file types (name and extension) into which the currently used version of Matplotlib
    # can export its plot. This information is read out from Matplotlib once on application start (in view/__init__.py,
    # Window initialization) and the filter object is constructed there.
    dialog.setNameFilters(self.filters)
    dialog.selectNameFilter(self.filters[1])

    dialog.setWindowTitle("Save plot")
    dialog.setDirectory(str(Path.home()))
    dialog.setAcceptMode(QFileDialog.AcceptMode.AcceptSave)
    if dialog.exec():
        # This logic ensures that the file extension is added to the file's name if the user doesn't manually add it
        fn = dialog.selectedFiles()[0]
        ext = os.path.splitext(fn)
        ff = dialog.selectedNameFilter()
        extf = ""
        for ft in self.supported_filetypes:
            if ft in ff:
                extf = self.supported_filetypes[ft]
                break

        finalpath = fn if ext[1][1:] in extf else fn + "." + extf[0]

        # saving the export to the specified location
        try:
            mpl_plt.savefig(finalpath)
        except Exception as err:  # fn disk space? No permissions? Shows the error in an information popup window.
            box = QMessageBox()
            box.setWindowTitle("Error!")
            box.setText("Saving didn't work! The following error occurred:")
            box.setInformativeText(str(err))
            box.exec()
            return


def export_to_json(self):
    """
    Called when the user clicks selects "Export to JSON" in the menu. Opens the location selection dialog and tries to
    save the data into the selected directory.
    :param self: Reference to window
    """
    dialog = QFileDialog(self)

    # self.filters contains all the file types (name and extension) into which the currently used version of Matplotlib
    # can export its plot. This information is read out from Matplotlib once on application start (in view/__init__.py,
    # Window initialization) and the filter object is constructed there.
    dialog.setNameFilters(["*.json (JavaScript Object Notation)"])
    dialog.selectNameFilter(self.filters[0])

    dialog.setWindowTitle("Save to JSON")
    dialog.setDirectory(str(Path.home()))
    dialog.setAcceptMode(QFileDialog.AcceptMode.AcceptSave)
    if dialog.exec():
        # This logic ensures that the file extension is added to the file's name if the user doesn't manually add it
        fn = dialog.selectedFiles()[0]
        ext = os.path.splitext(fn)
        finalpath = fn if str.lower(ext[1][1:]) == "json" else fn + ".json"

        # saving the export to the specified location
        try:
            fgph.save_as_json(self.model, finalpath)
        except Exception as err:  # fn disk space? No permissions? Shows the error in an information popup window.
            box = QMessageBox()
            box.setWindowTitle("Error!")
            box.setText("Saving didn't work! The following error occurred:")
            box.setInformativeText(str(err))
            box.exec()
            return


def plmodel_dialog(self):
    """
    Called when the user clicks selects "Set pathloss model" in the menu. Opens the custom dialog to select the new
    pathloss model and set the parameters. The layout for the custom dialog is defined here. Checks the user input
    for validity, then sets the specified pathloss model.
    :param self: Reference to window
    """

    class PLModelDialog(QDialog):
        """
        Inner class which contains layout and logic of the custom dialog window.
        """

        def __init__(self, parent, plmodel: fgdg.PLModel, flag):
            """
            Sets the dialog layout and pre-selects the currently active pathloss model type.
            Also contains the logic to create the documentation windows which appear when the "?" buttons are clicked
            :param parent: reference to window
            :param plmodel: currently active pathloss model
            :param flag: QDialog Window flag as defined here: https://doc.qt.io/qt-5/qt.html#WindowType-enum
            """
            super(PLModelDialog, self).__init__(parent, flag)
            self.parent = parent
            self.currentPLModel = plmodel
            self.setWindowTitle("Set path loss model")
            lay = QVBoxLayout(self)

            lay.addWidget(QLabel("Select the path loss model to use:"), alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
            selection_widget = QWidget(self, QtCore.Qt.WindowType.Widget)
            selection_widget_lay = QVBoxLayout(selection_widget)

            class ListEntry(QWidget):
                """
                Entry in the path loss model choice list. Each choice contains a radio button and a "?" button which
                opens a window with documentation.
                """

                def __init__(self, plmodeltype: Type[fgdg.PLModel]):
                    """
                    Layout and event handler setting for the ListEntry
                    :param plmodeltype: the type of path loss model associated with the list entry. Don't use PLModel
                    abstract class, only concrete subclasses!
                    """
                    super().__init__()
                    self.plmodeltype = plmodeltype  # which path loss model type does this list entry belong to?

                    # noinspection PyTypeChecker
                    self.radio = QRadioButton(self.plmodeltype.ui_title)  # set the title
                    icon = QtGui.QIcon('assets/questionmark.svg')
                    self.button = QPushButton(icon, "")
                    self.button.setSizePolicy(QSizePolicy(QSizePolicy.Policy.Fixed, QSizePolicy.Policy.Fixed))
                    self.button.clicked.connect(self.buttonClicked)

                    layout = QHBoxLayout()
                    layout.addWidget(self.button)
                    layout.addWidget(self.radio)
                    layout.setContentsMargins(0, 0, 0, 0)
                    self.setLayout(layout)

                def buttonClicked(self):
                    """
                    Callback function which is executed when a "?" button is clicked. Creates the documentation window.
                    """

                    class HelpTextDialog(QDialog):
                        """
                        The documentation window class.
                        """

                        def __init__(self, plmodeltype: Type[fgdg.PLModel]):
                            """
                            Sets the layout and content for the documentation window.
                            :param plmodeltype: the type of path loss model associated with the list entry. Don't use
                            PLModel abstract class, only concrete subclasses!
                            """
                            super().__init__()
                            self.setMinimumWidth(600)
                            # noinspection PyTypeChecker
                            self.setWindowTitle(plmodeltype.ui_title)  # retrieve plmodel title from plmodel class
                            layout = QVBoxLayout()
                            self.setLayout(layout)
                            # noinspection PyTypeChecker
                            text = QLabel(plmodeltype.ui_documentation, self)  # retrieve doc text from plmodel class
                            text.setTextFormat(QtCore.Qt.TextFormat.RichText)  # doc text is HTML formatted (rich text)
                            text.setWordWrap(True)  # break lines
                            layout.addWidget(text)

                            self.button = QDialogButtonBox(
                                QDialogButtonBox.StandardButton.Ok)  # ok button closes doc window
                            self.button.accepted.connect(self.close)
                            layout.addWidget(self.button, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)

                    # noinspection PyTypeChecker
                    docdialog = HelpTextDialog(self.plmodeltype)
                    docdialog.exec()

            # each path loss model has its own entry in the list
            self.le_fspl = ListEntry(fgdg.FSPLModel)
            self.le_abg = ListEntry(fgdg.ABGPLModel)
            self.le_ci = ListEntry(fgdg.CIPLModel)
            self.le_infsh = ListEntry(fgdg.InFSHPLModel)

            # the radio buttons are grouped, to ensure that only one can be selected at a time
            self.group = QButtonGroup()
            self.group.addButton(self.le_fspl.radio)
            self.group.addButton(self.le_abg.radio)
            self.group.addButton(self.le_ci.radio)
            self.group.addButton(self.le_infsh.radio)

            # set event handler
            self.le_fspl.radio.toggled.connect(self.updateInputLineEdits)
            self.le_abg.radio.toggled.connect(self.updateInputLineEdits)
            self.le_ci.radio.toggled.connect(self.updateInputLineEdits)
            self.le_infsh.radio.toggled.connect(self.updateInputLineEdits)

            # PLParameters go into this Grid Layout
            self.inputWidget = QWidget(self, QtCore.Qt.WindowType.Widget)
            self.inputWidgetLay = QGridLayout(self.inputWidget)

            selection_widget_lay.addWidget(self.le_fspl, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
            selection_widget_lay.addWidget(self.le_abg, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
            selection_widget_lay.addWidget(self.le_ci, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
            selection_widget_lay.addWidget(self.le_infsh, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)

            lay.addWidget(selection_widget, alignment=QtCore.Qt.AlignmentFlag.AlignVCenter)

            lay.addWidget(QLabel("And set the neccessary parameters:"), alignment=QtCore.Qt.AlignmentFlag.AlignLeft)

            lay.addWidget(self.inputWidget)

            if isinstance(self.currentPLModel, fgdg.FSPLModel):  # pre-select the currently active pathloss model type
                self.le_fspl.radio.setChecked(True)
            elif isinstance(self.currentPLModel, fgdg.ABGPLModel):
                self.le_abg.radio.setChecked(True)
            elif isinstance(self.currentPLModel, fgdg.CIPLModel):
                self.le_ci.radio.setChecked(True)
            elif isinstance(self.currentPLModel, fgdg.InFSHPLModel):
                self.le_infsh.radio.setChecked(True)
            else:
                raise RuntimeWarning("Dialog window received unknown path-loss model")

            self.buttons = QDialogButtonBox(QDialogButtonBox.StandardButton.Ok |
                                            QDialogButtonBox.StandardButton.Cancel)
            self.buttons.accepted.connect(self.accepted)  # set callback function for when the "OK" button is clicked
            self.buttons.rejected.connect(self.rejected)  # set callback function for when "Cancel" is clicked
            lay.addWidget(self.buttons, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)

        def get_selected_plmodeltype(self):
            """
            Returns the type ocject corresponding to the path loss model type which is currently selected via the
            radio buttons.
            :return: path loss model type
            """
            if self.le_fspl.radio.isChecked():
                return fgdg.FSPLModel
            elif self.le_abg.radio.isChecked():
                return fgdg.ABGPLModel
            elif self.le_ci.radio.isChecked():
                return fgdg.CIPLModel
            elif self.le_infsh.radio.isChecked():
                return fgdg.InFSHPLModel
            else:
                raise RuntimeWarning("get_selected_plmodeltype received unknown path loss model")

        def updateInputLineEdits(self, event):
            """
            This callback function is triggered when a different pathloss radio button is selected.
            It reconstructs the labels and input elements for the parameters.
            :param event: event object
            """
            plmodel_type = self.get_selected_plmodeltype()

            # clear grid
            while self.inputWidgetLay.count():
                widget = self.inputWidgetLay.takeAt(0).widget()
                widget.setParent(None)

            # and reconstruct row by row
            cur_row = -1  # current row
            # paramset contains the configuration of the currently active path loss model
            paramset = self.currentPLModel.paramSet

            # each paramProfile contains information about one parameter of the newly selected pathloss model, which
            # is about to be constructed. It contains default, min and max values for the parameter, a descriptive
            # label and a "displayfactor". This is a number by which the parameter is divided before display.
            # e.g. in the data structure, frequency is 3e9 Hz, but in the GUI it is displayed as 3 GHz, so the
            # displayfactor is 1e9.
            for key, param in plmodel_type.paramProfiles.items():
                cur_row = cur_row + 1
                self.inputWidgetLay.addWidget(QLabel(param.label), cur_row, 0)  # label

                # if applicable, display the currently active value instead of the default value
                if type(self.currentPLModel) == self.get_selected_plmodeltype() and key in paramset:
                    display_value = paramset[key]
                else:
                    display_value = param.default

                # divide the value by the displayfactor for display
                display_value = display_value / param.displayfactor

                le_parameter = QLineEdit("{:.4f}".format(display_value),
                                         self.inputWidget)  # LineEdit box

                # in order to allow the association of each LineEdit box with its parameter (later, when the new
                # inputs are read out and the pl model is constructed), we store this information in a custom object
                # parameter called 'paramkey'
                le_parameter.setProperty('paramkey', key)

                # validator
                # The input fields should constrain inputs as much as possible to only allow the input of values within
                # the min and max range of each parameter. Unfortunately, this doesn't work right now, for whatever
                # reason
                val = QtGui.QDoubleValidator(param.min / param.displayfactor, param.max / param.displayfactor, 4,
                                             le_parameter)
                le_parameter.setValidator(val)

                self.inputWidgetLay.addWidget(le_parameter, cur_row, 1)

        def accepted(self):
            """
            Callback function that is executed when the "OK" button is clicked in the pathloss model dialog window.
            Reads values and tries to set a path loss model. If unsuccessful, forwards error message to the user.
            """
            try:
                # collect all inputs and set them as path loss model
                le_values = []
                num_params = len(self.get_selected_plmodeltype().paramProfiles)
                # for each parameter ...
                for param_pos in range(0, num_params):
                    le_widget = self.inputWidgetLay.itemAtPosition(param_pos, 1).widget()  # get the LineEdit box
                    paramkey = le_widget.property('paramkey')  # read out the associated pl model parameter
                    # get the profile of the parameter (incl default, min, max values and so on)
                    paramprofile = self.get_selected_plmodeltype().paramProfiles[paramkey]

                    # replace mistyped "," decimal separator by . If this doesn't work, it will also raise a ValueError.
                    inputval = float(le_widget.text().replace(',', '.'))
                    # multiply the input value (e.g. 3 GHz) with the displayfactor (e.g. 1e9) to get the data structre
                    # value (3e9 Hertz)
                    inputval = inputval * paramprofile.displayfactor

                    # collect all input values
                    le_values.append(inputval)

                # try to create the pl model. If any value is out of range, they will raise a ValueError with info.
                if self.le_fspl.radio.isChecked():
                    self.parent.model.plmodel = fgdg.FSPLModel(*le_values)
                elif self.le_abg.radio.isChecked():
                    self.parent.model.plmodel = fgdg.ABGPLModel(*le_values)
                elif self.le_ci.radio.isChecked():
                    self.parent.model.plmodel = fgdg.CIPLModel(*le_values)
                elif self.le_infsh.radio.isChecked():
                    self.parent.model.plmodel = fgdg.InFSHPLModel(*le_values)
                else:
                    # If this happened, you forgot to add your freshly implemented PLModel to the list if-chain above
                    raise RuntimeWarning("Dialog window: No (or unknown) path-loss model was checked")
            except ValueError as err:  # message box for invalid input
                msg_box = QMessageBox()
                msg_box.setWindowTitle("Input error")

                error_msg = str(err)
                if error_msg.__contains__('convert'):
                    error_msg = 'Your input is formatted incorrectly.'
                msg_box.setText(error_msg)
                msg_box.exec()
                return
            self.close()

        def rejected(self):
            """
            Callback function that is executed when the "Abort" button is clicked in the pathloss model dialog window.
            Closes the window without applying changes.
            """
            self.close()

    # The dialog window is constructed using the class above, and executed.
    dialog = PLModelDialog(self, self.model.plmodel, QtCore.Qt.WindowType.Dialog)
    dialog.exec()

def snrparams_dialog(self):
    """
    Called when the user clicks selects "Set SNR params" in the menu. Opens the custom dialog to change noise and power.
    The layout for the custom dialog is defined here. Checks the user input
    for validity, then sets the specified SNR params.
    :param self: Reference to window
    """

    class SNRParamsDialog(QDialog):
        """
        Inner class which contains layout and logic of the custom dialog window.
        """

        def __init__(self, parent, power: float, noise: float, flag):
            """
            Sets the dialog layout and fills the text boxes with the power and noise given as input.
            :param parent: reference to window
            :param power: currently set power
            :param noise: currently set noise
            :param flag: QDialog Window flag as defined here: https://doc.qt.io/qt-5/qt.html#WindowType-enum
            """
            super(SNRParamsDialog, self).__init__(parent, flag)
            self.parent = parent
            self.currentPower = power
            self.currentNoise = noise
            self.setWindowTitle("Set SNR parameters")
            lay = QVBoxLayout(self)

            lay.addWidget(QLabel("Set the parameters to use for SNR calculation:"),
                          alignment=QtCore.Qt.AlignmentFlag.AlignLeft)

            inputWidget = QWidget(self, QtCore.Qt.WindowType.Widget)
            inputWidgetLay = QGridLayout(inputWidget)
            lay.addWidget(inputWidget)

            # power
            inputWidgetLay.addWidget(QLabel("Power [dBm]:"), 0, 0)  # label to upper left corner of grid
            self.power_le = QLineEdit("{:.4f}".format(self.currentPower), inputWidget)
            self.power_val = QtGui.QDoubleValidator(-100, 120, 4, self.power_le)
            self.power_le.setValidator(self.power_val)
            inputWidgetLay.addWidget(self.power_le, 0, 1)

            # noise
            inputWidgetLay.addWidget(QLabel("Noise [dBm]:"), 1, 0)
            self.noise_le = QLineEdit("{:.4f}".format(self.currentNoise), inputWidget)
            self.noise_val = QtGui.QDoubleValidator(-100, 120, 4, self.noise_le)
            self.power_le.setValidator(self.noise_val)
            inputWidgetLay.addWidget(self.noise_le, 1, 1)

            # buttons and callbacks
            self.buttons = QDialogButtonBox(QDialogButtonBox.StandardButton.Ok |
                                            QDialogButtonBox.StandardButton.Cancel)
            self.buttons.accepted.connect(self.accepted)  # set callback function for when the "OK" button is clicked
            self.buttons.rejected.connect(self.rejected)  # set callback function for when "Cancel" is clicked
            lay.addWidget(self.buttons, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)


        def accepted(self):
            """
            Callback function that is executed when the "OK" button is clicked in the SNR params dialog window.
            Reads values and tries to set them in the model. If unsuccessful, forwards error message to the user.
            """
            try:
                self.parent.model.power = float(self.power_le.text().replace(',', '.'))
                self.parent.model.noise = float(self.noise_le.text().replace(',', '.'))
            except ValueError as err:  # message box for invalid input
                msg_box = QMessageBox()
                msg_box.setWindowTitle("Input error")

                error_msg = str(err)
                msg_box.setText(error_msg)
                msg_box.exec()
                return
            self.close()

        def rejected(self):
            """
            Callback function that is executed when the "Abort" button is clicked in the SNR params dialog window.
            Closes the window without applying changes.
            """
            self.close()

    # The dialog window is constructed using the class above, and executed.
    dialog = SNRParamsDialog(self, self.model.power, self.model.noise, QtCore.Qt.WindowType.Dialog)
    dialog.exec()

def heatmap_zpos_dialog(self):
    """
    Called when the user clicks selects "Set heatmap zpos" in the menu. Opens a dialog to enter the new desired zpos
    (= reference height for the heatmap). When the user clicks "OK", the new zpos is forwarded to MatPlotView to
    apply.
    :param self: Reference to window
    """
    heatmap_zpos, ok = QInputDialog.getDouble(self, 'Set heatmap zpos', 'Enter new height in [m]',
                                              self.canvas.heatmap_zpos, 0, decimals=4)
    if ok:
        self.canvas.updateHeatmapZpos(heatmap_zpos)


def factorysize_dialog(self):
    """
    Called when the user selects "Set factory size" in the menu. Opens the custom dialog to set the new
    factory size. The layout for the custom dialog is defined here. Checks the user input for validity, then sets the
    specified factory size.
    :param self: Reference to window
    """

    class FactorySizeDialog(QDialog):
        """
        Inner class which contains layout and logic of the custom dialog window.
        """

        def __init__(self, parent, factory: fg.Factory, flag):
            """
            Sets the dialog layout and sets the dimensions of the current factory as default input.
            :param parent: reference to window
            :param factory: currently active factory with its dimensions
            :param flag: QDialog Window flag as defined here: https://doc.qt.io/qt-5/qt.html#WindowType-enum
            """
            super(FactorySizeDialog, self).__init__(parent, flag)
            self.parent = parent
            self.setWindowTitle("Set factory size")
            lay = QVBoxLayout(self)

            lay.addWidget(QLabel("Set the new factory size in [m]."), alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
            lay.addWidget(QLabel("Warning: All actors outside the new boundaries will be removed!"),
                          alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
            inputWidget = QWidget(self, QtCore.Qt.WindowType.Widget)
            inputWidgetLay = QGridLayout(inputWidget)

            self.le_x = QLineEdit(str(factory.x), inputWidget)
            self.le_y = QLineEdit(str(factory.y), inputWidget)
            self.le_z = QLineEdit(str(factory.z), inputWidget)

            self.le_x.textEdited.connect(self.onEditFieldStringChanged)
            self.le_y.textEdited.connect(self.onEditFieldStringChanged)
            self.le_z.textEdited.connect(self.onEditFieldStringChanged)

            val = QtGui.QDoubleValidator(1, 1000, 4)  # validator accepts floats inputs between 1 and 1000 [meters]
            val.setLocale(QtCore.QLocale(QtCore.QLocale.Language.English, QtCore.QLocale.Country.UnitedStates))
            self.le_x.setValidator(val)  # for each dimension
            self.le_y.setValidator(val)
            self.le_z.setValidator(val)

            inputWidgetLay.addWidget(QLabel("width (x)"), 0, 0)
            inputWidgetLay.addWidget(self.le_x, 1, 0)
            inputWidgetLay.addWidget(QLabel("length (y)"), 0, 1)
            inputWidgetLay.addWidget(self.le_y, 1, 1)
            inputWidgetLay.addWidget(QLabel("height (z)"), 0, 2)
            inputWidgetLay.addWidget(self.le_z, 1, 2)
            lay.addWidget(inputWidget, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)

            self.buttons = QDialogButtonBox(QDialogButtonBox.StandardButton.Ok |
                                            QDialogButtonBox.StandardButton.Cancel)
            self.buttons.accepted.connect(self.accepted)  # set callback function for when the "OK" button is clicked
            self.buttons.rejected.connect(self.rejected)  # set callback function for when "Cancel" is clicked
            lay.addWidget(self.buttons, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)

        def accepted(self):
            """
            Callback function that is executed when the "OK" button is clicked in the factory size dialog window.
            Sets the factory in the model to the specified size.
            """
            self.parent.model.set_factory_size(float(self.le_x.text()), float(self.le_y.text()),
                                               float(self.le_z.text()))
            self.close()

        def rejected(self):
            """
            Callback function that is executed when the "Abort" button is clicked in the factory size dialog window.
            Closes the window without applying changes.
            """
            self.close()

        def onEditFieldStringChanged(self) -> None:
            """
            Replaces all "," input chars by "." to have the input comply to the double validator (which is set to US
            English)
            """
            sender = QtCore.QObject.sender(self)
            if isinstance(sender, QLineEdit):
                sender.setText(sender.text().replace(",", "."))

    factory = self.model.get_factory()  # retrieve current factory (with dimensions) from the model

    # The dialog window is constructed using the class above, and executed.
    dialog = FactorySizeDialog(self, factory, QtCore.Qt.WindowType.Dialog)
    dialog.exec()


def bgimage_dialog(self):
    """
    Called when the user selects "Set background image" in the menu. Creates the file selection dialog, reads selected
    file, loads the image as background. Displays error when image isn't parsable.
    :param self: reference to window
    """

    filename = QFileDialog.getOpenFileName(self, "Select image", str(Path.home()), "Images (*.png *.bmp *.jpg)")

    if not filename[0]:
        return  # loading aborted

    try:
        img = Image.open(filename[0])  # try to read the image file
    except Exception as err:  # file not readable or wrong format? Show error message in pop-up windoe
        box = QMessageBox()
        box.setWindowTitle("Error!")
        box.setText("Loading didn't work! The following error occurred:")
        box.setInformativeText(str(err))
        box.exec()
        return

    self.canvas.setBackgroundImage(img)
    return


def about_dialog(self):
    """
    Called when the user selects "About" in the menu. Opens a dialog with version and license information.
    :param self: Reference to window
    """

    class AboutDialog(QDialog):
        """
        Inner class which contains layout and logic of the about dialog window.
        """

        def __init__(self, parent, flag):
            """
            Sets the dialog layout.
            :param parent: reference to window
            :param flag: QDialog Window flag as defined here: https://doc.qt.io/qt-5/qt.html#WindowType-enum
            """
            super(AboutDialog, self).__init__(parent, flag)
            self.parent = parent
            self.setWindowTitle("About")

            # read license file
            with open('LICENSE') as f:
                licensetext = f.readlines()

            lay = QVBoxLayout(self)
            title = QLabel("5G-FINS")
            title.setStyleSheet("font-size: 24px;")
            lay.addWidget(title, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
            lay.addWidget(QLabel("v" + fg.__version__), alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
            # lay.addWidget(QLabel("Test build! For internal purposes only!"),
            #               alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)
            lay.addWidget(QLabel("License:"), alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
            licensebox = QPlainTextEdit()
            licensebox.setPlainText(''.join(licensetext))
            licensebox.setReadOnly(True)
            lay.addWidget(licensebox)

            self.buttons = QDialogButtonBox(QDialogButtonBox.StandardButton.Ok)
            self.buttons.accepted.connect(self.close)
            lay.addWidget(self.buttons, alignment=QtCore.Qt.AlignmentFlag.AlignHCenter)

        def sizeHint(self) -> QtCore.QSize:
            return QtCore.QSize(570, 400)

    dialog = AboutDialog(self, QtCore.Qt.WindowType.Dialog)
    dialog.exec()
