# -----------------------------------------------------------
# contains the code related to the main widget: The plot view that represents the data on a plotting surface.
# Including: Drawing, event handlers, tool behavior on the plotting surface
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------
import math

import numpy as np
import fgfins as fg
from PyQt6.QtWidgets import *
from PyQt6 import QtCore
from PyQt6.QtCore import QObject, QThread, pyqtSignal
from PyQt6 import QtGui
from PyQt6.QtGui import QPalette
import PIL.Image as pil_image
import matplotlib
from matplotlib.collections import QuadMesh
import matplotlib.pyplot as mpl_plt
import matplotlib.colors as mpl_colors
import matplotlib.offsetbox as mpl_offsetbox
import fgfins.datagenerator as nodg
import matplotlib.figure
import matplotlib.lines
import matplotlib.legend
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
import typing


class MatPlotView(QAbstractItemView):
    """
    Contains all functionality related to the MatPlotlib plot.
    """

    def __init__(self, window):
        """
        Initialization of all sorts of parameters which are shared between the functions of the class.
        :param window: Reference to the embedding window that contains this plot. This is needed to e.g. read out
        which tool (Mouse tool, Add RD tool, ...) is currently selected, and act appropriately.
        """
        super(QAbstractItemView, self).__init__()
        self.window = window
        self.dpi = self.getDpi()

        # self.dpi = self.getDpi()  # optional: set plot dpi to environment dpi. Turned off
        self._fig, self._axis = mpl_plt.subplots(1, 2, gridspec_kw={"width_ratios": [5, 95]},
                                                 figsize=(5.5, 4),
                                                 dpi=self.dpi
                                                 )  # ratio of colorbar and heatmap

        # set types for type checking. Useful for code completion features in the IDE.
        fig_type = typing.NewType('fig_type', matplotlib.figure.Figure)
        axes_type = typing.NewType('axes_type', matplotlib.figure.Axes)
        colormesh_type = typing.NewType('colormesh_type', QuadMesh)
        worker_type = typing.NewType('colormesh_type', Worker)
        annbox_type = typing.NewType('annbox_type', mpl_offsetbox.AnchoredOffsetbox)
        self._fig = fig_type(self._fig)
        self._colorbar = axes_type(self._axis[0])
        self._heatmap = axes_type(self._axis[1])
        self._colormesh = colormesh_type(mpl_plt.pcolormesh([[0], [0]]))  # just some default, will be overwritten
        self._ann = annbox_type(mpl_offsetbox.AnchoredOffsetbox('center'))  # just some default, will be overwritten
        self._worker = worker_type(Worker(None, list(), 0, 0, 0, list(), QtCore.QMutex()))

        # Flags for diffent purposes
        self._label_visibility = True  # show labels and legend
        self._dontclear = False  # to resolve conflict in mouse tool pick vs. click event
        self._layerqos = True  # show qos layer
        self._interactivemode = True  # show delay layer
        self._bgimagemode = True  # show background image
        self._select_multiple = False  # True when in multi-select mode (i.e. ctrl button held down)

        # render background transparent (= window color) and remove border
        self._fig.patch.set_facecolor(QWidget.palette(self).color(QPalette.ColorRole.Window).getRgbF())
        self.setStyleSheet("background-color: rgba(0,0,0,0)")

        self.standard_height_new_rds = 3
        self.standard_height_new_ues = 1.5
        self.heatmap_zpos = 3  # meters height

        self.plot_colorbar(just_set=True)

        self._canvas = FigureCanvasQTAgg(self._fig)  # the actual matplotlib canvas
        self._canvas.setParent(self)

        # colors for actors in different modes
        self.rdcolor = 'black'
        self.uecolor = '#59b5ff'  # for the blue triangles
        self.uegreen = 'g'  # when a condition is (req SINR) is fulfilled
        self.uered = '#8C000F'  # when it isn't

        self._legend = self._redraw_labels()  # will be redrawn anyway

        self.canvas_onpick = None  # canvas event handlers set by set_tool_eventhandlers() on tool selection
        self.canvas_onclick = None
        self.canvas_onrelease = None
        self.canvas_onmotion = None

        # the interior items of the view are organized in a box layout, to make them expand to full size.
        # The canvas (== the matplotlib object) is added to this layout.
        _innerLayout = QHBoxLayout(self)
        _innerLayout.setContentsMargins(0, 0, 0, 0)
        _innerLayout.setSpacing(0)
        _innerLayout.addWidget(self._canvas)

        # this list holds pairs of [model reference index, associated line in the plot]
        # this is how we keep track which line belongs to which item (actor, wall) in the data model
        self._itemlines = list()

        # Information for the addWall tool. Where is the starting position? Is the drawing method restrictive?
        self._wallStartPos = [0, 0]  # coordinates [x, y]
        self._tempWall = mpl_plt.Line2D([0], [0])  # object holding the line representing the wall
        self._wallHorizontal = False  # flag to only draw horizontal walls
        self._wallVertical = False  # flag to only draw vertical walls

        # progressbar object to show during calculation
        self.progressbar = None
        self.pl_calc_thread = QThread(parent=self.window)
        self.pl_calc_mutex = QtCore.QMutex()

        self.pathloss_matrix = np.zeros(shape=(0, 0))  # empty pl matrix
        self.minpl = 1  # minimum path loss value (to prevent the plot from showing white spots where the pl is zero)

        self.bgimg = pil_image.Image()  # background image

    def init_after_setmodel(self):
        """
        Some additional initialization steps that can only be completed after the data model has been connected to the
        MatPlotView (which happens in the script that creates the MatPlotView object). So the calling order is:
        1. create MatPlotView object, 2. connect data model to object, 3. call this function
        This function draws the initial blank white heatmap and sets appropriate limits for the initial scales.
        """
        factory = self.model().get_factory()
        self.plot_heatmap(np.empty(shape=[0, 0]))
        self._heatmap.clear()
        self._heatmap.set_xlim(0, factory.x)
        self._heatmap.set_ylim(0, factory.y)

        # sets the correct asect ratio for the plot (by plotting an empty image in the background)
        self._axis[1].imshow([[[0, 0, 0, 0]]], extent=[0, factory.x, 0, factory.y])

        self.model().factory_size_changed.connect(self.onFactorySizeChanged)
        self.model().plmodel_changed.connect(self.redrawView)
        self.model().snrparams_changed.connect(self.redrawHeatmap)

    def updateHeatmapZpos(self, zpos: float, redraw=True):
        """
        Set a new height (z-position) to which the colors in the heatmap should refer to. Optionally redraw everything.
        :param zpos: new height for the heatmap
        :param redraw: whether to redraw the heatmap afterwards
        """
        self.heatmap_zpos = zpos
        if redraw:
            self.redrawView()

    def onFactorySizeChanged(self, data_changed: bool):
        """
        Callback function that is called when the factory size was changed.
        :param data_changed: True if data changed alongside the factory dimensions (i.e. items were out of bounds and
        deleted)
        Ensures that the heatmap zpos is within the new factory limits and redraws everything.
        """
        factory = self.model().get_factory()
        if self.heatmap_zpos > factory.z:
            self.updateHeatmapZpos(factory.z, redraw=False)
        if not data_changed:
            self.redrawView()

    @staticmethod
    def getDpi() -> float:
        """
        Get environment pixel density (dots per inch) from Qt
        :return: dpi value
        """
        dpi = QtGui.QGuiApplication.primaryScreen().physicalDotsPerInch()
        return dpi

    def visualRegionForSelection(self, selection: QtCore.QItemSelection) -> QtGui.QRegion:
        # not actually used, just to hide a "not implemented" exception
        return QtGui.QRegion()

    def indexAt(self, p: QtCore.QPoint) -> QtCore.QModelIndex:
        # not actually used, just to hide a "not implemented" exception
        return QtCore.QModelIndex()

    def visualRect(self, index: QtCore.QModelIndex) -> QtCore.QRect:
        # not actually used, just to hide a "not implemented" exception
        return QtCore.QRect()

    def verticalOffset(self) -> int:
        # not actually used but necessary for QAbstractItemView
        return 0

    def horizontalOffset(self) -> int:
        # not actually used but necessary for QAbstractItemView
        return 0

    def moveCursor(self, QAbstractItemView_CursorAction, Qt_KeyboardModifier):
        # not actually used but necessary for QAbstractItemView
        return QtCore.QModelIndex()

    def dataChanged(self, topLeft: QtCore.QModelIndex, bottomRight: QtCore.QModelIndex,
                    roles: typing.Iterable[int] = ..., **kwargs) -> None:
        """
        Called by Qt Model/View message system each time data in the associated model changes.
        Initiates complete redrawing of the view - unless only a User Entity was added. If so, redrawView will skip the
        recalculation of the heatmap
        See https://doc.qt.io/qt-5/qabstractitemmodel.html#dataChanged
        :param topLeft:     First index impacted by the change
        :param bottomRight: Last index impacted by the change
        :param roles:       To specify which data roles have been motified
        """

        just_a_ue = bool(fg.Roles.AddedSingleUE in roles)
        self.redrawView(redraw_heatmap=not just_a_ue)


    def redrawView(self, redraw_heatmap=True, reset=False):
        """
        Redraws complete view, recalculates all layers. Very resource-intense, avoid whenever possible.
        Necessary when data is changed in the model (moved, added, removed).
        :param redraw_heatmap:  can be set to false to skip recalculation of the heatmap
        :param reset:           can be set to true to initiate a pure plot reset (delete heatmap and all items)
        """
        # with QtCore.QMutexLocker(self.redraw_view_mutex):
        # gather all data from model
        model = self.model()
        factory = model.get_factory()
        # each data item is associated an index by the model. We gather all indexes.
        pers_ind_list = model.persistentIndexList()

        # reset view
        if not self._interactivemode or not self.model().indexOfType(fg.RadioDot) or reset:
            # if there are fn items or we're not in interactive mode, clear the whole plot, which deletes
            # the heatmap and all actorlines, leaving a white plotting surface
            self._heatmap.clear()
        else:
            # if we're interactive and there are items, we don't want a white plotting surface as long as the
            # calculation is running. In this case, we have to remove the items by hand.
            for item in self._itemlines:
                item[1].remove()

        self._heatmap.set_xlim(0, factory.x)
        self._heatmap.set_ylim(0, factory.y)
        self._itemlines = []

        # and refill
        rd_list = []
        rd_in_set = False  # flag to check whether there is a Radio Dot

        for index in pers_ind_list:
            newitem = model.data(index)
            if isinstance(newitem, fg.RadioDot):
                # Radio Dots get the circle shape
                self._itemlines.append([index, self._heatmap.plot(newitem.xpos, newitem.ypos, 'o', picker=True,
                                                                  pickradius=7, color=self.rdcolor)[0]])
                rd_list.append(newitem)
                rd_in_set = True
            elif isinstance(newitem, fg.UserEndpoint):
                # User Endpoints get the triangle shape
                self._itemlines.append([index, self._heatmap.plot(newitem.xpos, newitem.ypos, '^', picker=True,
                                                                  pickradius=7)[0]])
                # and a color according to the current layer and their state
                self.colorize_ue(self._itemlines[-1])
            elif isinstance(newitem, fg.Wall):
                self._itemlines.append([index, self._heatmap.plot([newitem.startx, newitem.endx],
                                                                  [newitem.starty, newitem.endy],
                                                                  'k-', linewidth=3, picker=True, pickradius=7)[0]])

        if self._interactivemode and redraw_heatmap and rd_in_set and not reset:
            # recalculate heatmap values
            factory = self.model().get_factory()
            wall_indexes = self.model().indexOfType(fg.Wall)
            wall_list = list(map(self.model().data, wall_indexes))

            # show progress bar window during heatmap calculation
            self.progressbar = QProgressDialog("Heatmap calculation in progress ...",
                                       "Abort", 0, math.ceil(factory.x) * math.ceil(factory.y),
                                               flags=QtCore.Qt.WindowType.WindowStaysOnTopHint)
            self.progressbar.setWindowTitle("5G Network Monitor")
            # all background windows are blocked from input
            self.progressbar.setWindowModality(QtCore.Qt.WindowModality.ApplicationModal)
            # if calculation takes longer than 1 second, the progressbar appears
            self.progressbar.setMinimumDuration(1000)
            self.progressbar.canceled.connect(self.onProgressBarCanceled)

            # calculate path loss matrix in a separate thread to prevent GUI freezing
            self.pl_calc_thread = QThread(parent=self.window)

            # the call to the matrix calculation is in the Worker class routine
            self._worker = Worker(self.model().plmodel, rd_list, factory.x, factory.y,
                            self.heatmap_zpos, wall_list, self.pl_calc_mutex)
            self._worker.moveToThread(self.pl_calc_thread)
            self.pl_calc_thread.started.connect(self._worker.run)
            self._worker.progress.connect(self.updateProgressbar)
            self._worker.finished.connect(self.redrawHeatmap)
            self.window.setEnabled(False)
            self.pl_calc_thread.start()  # start thread

        # redraw labels if appropriate
        if self._label_visibility:
            self._redraw_labels(draw=True)

        # set selection visuals (dots/triangles large or small) according to the selection model
        self.selectionChanged()

        # plot the background image
        if self._bgimagemode:
            self.plotBackgroundImage()

        # sets the correct asect ratio for the plot (by plotting an empty image in the background)
        self._axis[1].imshow([[[0, 0, 0, 0]]], extent=[0, factory.x, 0, factory.y])

        # initiate the drawing of the updated canvas in MatPlotLib
        self._canvas.draw()

    def plotBackgroundImage(self):
        """
        If a background image was set, this function plots it over the whole plotting surface.
        """
        if self.bgimg.height != 0:  # if background was set
            factory = self.model().get_factory()
            self._axis[1].imshow(self.bgimg, extent=[0, factory.x, 0, factory.y])

    def onProgressBarCanceled(self):
        """
        Callback function which is called when the recalculation of the heatmap is aborted by pressing the cancel button
        in the progress bar window. Signals the thread which calculates the heatmap to stop calculation. The progressbar
        window is then automatically closed. Furthermore, interactive mode is turned off.
        """
        self.pl_calc_mutex.lock()
        self._worker.stopped = True
        self.pl_calc_mutex.unlock()
        self.window.setEnabled(True)
        self.window.interactivecb.setCheckState(QtCore.Qt.CheckState.Unchecked)

    def redrawHeatmap(self, pathloss_matrix: typing.Optional[np.ndarray] = None):
        """
        Draws a matrix of (previously calculated) heatmap values to the plot and adapts the colorbar. This function is
        called by the heatmap calculation worker thread when it has successfully finished its calculations.
        :param pathloss_matrix: 2D matrix of heatmap values
        """
        if pathloss_matrix is not None:
            self.pathloss_matrix = pathloss_matrix
            self.minpl = self.model().plmodel.calculate_pathloss(1)

        drawmatrix = None
        if self.pathloss_matrix != np.zeros(shape=(0, 0)):
            if self.window.cur_metric == fg.metrics["snr"]:
                drawmatrix = nodg.calculate_snr_matrix(self.pathloss_matrix, self.model().power, self.model().noise)
            if self.window.cur_metric == fg.metrics["pw"]:
                drawmatrix = nodg.calculate_power_matrix(self.pathloss_matrix, self.model().power)
            if self.window.cur_metric == fg.metrics["pl"]:
                drawmatrix = self.pathloss_matrix
            if drawmatrix is None:
                # if you see this, you forgot to enter your freshly generated metric into the list above
                raise ValueError('MatPlotView: Metric unknown')

            self.plot_heatmap(drawmatrix)

        self.plot_colorbar()
        self._redraw_labels(True)
        self._update_heatmap_visibility()
        self._canvas.draw()
        self.window.setEnabled(True)

    def updateProgressbar(self, progress_counter: int):
        """
        Callback function which is called when the path loss matrix calculation procedure signals a significant progress
        in its calcation. The definition of "significant" is specified by the programmer of that function. There should
        be at least a 1% change in progress during consecutive calls to this function.
        This function hands the given progress over to the progressbar GUI element for display.
        :param progress_counter:
        """
        self.pl_calc_mutex.lock()
        if not self._worker.stopped:
            self.progressbar.setValue(progress_counter)
        self.pl_calc_mutex.unlock()

    def setBackgroundImage(self, img: pil_image.Image):
        """
        Sets background image of the plotting surface to the specified image object and initiates plotting in case
        background plotting is turned on.
        @param img: Image to set as background.
        """
        self.bgimg = img
        if self._bgimagemode:
            self.plotBackgroundImage()
            self._canvas.draw()

    def selectionChanged(self, _=None, __=None) -> None:
        """
        Called by Qt Model/View message system each time the selection in the associated selection model changes.
        Sets the size of the markers according to the selection.
        See https://doc.qt.io/qt-5/qitemselectionmodel.html#selectionChanged
        """
        # get indexes of all selected items
        selection = self.selectionModel().selection().indexes()

        for itemline in self._itemlines:  # go through all items and their associated plot lines
            if itemline[0] in selection:  # if an item is selected
                itemline[1].set_markersize(10)  # draw its marker large (actors)
                itemline[1].set_linewidth(5)  # draw its line thick (wall)
            else:
                itemline[1].set_markersize(6)  # else draw it small
                itemline[1].set_linewidth(3)

    def keyPressEvent(self, a0: QtGui.QKeyEvent) -> None:
        """
        Callback method that gets called when any key is pressed and MatPlotView is in focus.
        Also, the window (view.py) forwards all Ctrl-key presses that it catches to this function.
        When Ctrl is pressed and mouse tool is selected, activates multi-selection.
        When Ctrl is pressed and Add Wall tool is selected, initiates restriction to horizontal walls.
        When Shift is pressed and Add Wall tool is selected, initiates restriction to vertical walls.
        When Del is pressed, all selected items are deleted.

        :param a0: event reference
        """
        if a0.key() == QtCore.Qt.Key.Key_Control:
            if self.window.cur_tool == fg.Tool.MOUSE:
                self._select_multiple = True
            elif self.window.cur_tool == fg.Tool.ADD_WALL:
                self._wallHorizontal = True
        if a0.key() == QtCore.Qt.Key.Key_Shift:
            self._wallVertical = True
        if a0.key() == QtCore.Qt.Key.Key_Delete:
            selected = self.selectionModel().selection().indexes()  # get selected items
            self.model().removeItems(selected)  # and delete them

    def keyReleaseEvent(self, a0: QtGui.QKeyEvent) -> None:
        """
        Callback method that gets called when any key is released and MatPlotView is in focus.
        Also, the window (view.py) forwards all Ctrl-key releases that it catches to this function.
        When Ctrl is released, deactivates multi-selection and horizontal wall drawing.
        When Shift is released, deactivates vertical wall drawing.
        :param a0: event reference
        """
        if a0.key() == QtCore.Qt.Key.Key_Control:
            self._select_multiple = False
            self._wallHorizontal = False
        if a0.key() == QtCore.Qt.Key.Key_Shift:
            self._wallVertical = False

    def cop_mouse(self, event):
        """
        Callback for the onPick event of the canvas when the mouse tool is selected.
        onPick is triggered when an item line is being clicked on. It's triggered *before* onClick handled in coc_mouse
        Updates the selection model to select the items which were picked.
        :param event: event object
        """
        for itemline in self._itemlines:  # go through all items and their associated plot lines
            if itemline[1] == event.artist:  # if this is the item which was clicked on
                if self._select_multiple:
                    # when Ctrl button is held, items are added or substracted from given selection
                    self.selectionModel().select(itemline[0], QtCore.QItemSelectionModel.SelectionFlag.Toggle)
                else:
                    # otherwise, the old selection is dismissed
                    self.selectionModel().select(itemline[0], QtCore.QItemSelectionModel.SelectionFlag.ClearAndSelect)
        self._canvas.draw()  # redraw the canvas with the updated selection

        # set dontclear flag to prevent the upcoming onClick event from undoing the selection
        self._dontclear = True

    def coc_mouse(self, _):
        """
        Callback for the onClick event of the canvas when the mouse tool is selected.
        onClick is triggered when the canvas is clicked upon.
        If an actor was clicked on, the onClick event is triggered *after* onPick handled in cop_mouse
        Updates the selection model to deselect all items, unless onPick was triggered before, or Ctrl is being pressed
        :param _: event object
        """
        if self._dontclear:
            self._dontclear = False  # release dontclear flag that was set by the onPick function
            return
        if not self._select_multiple:  # if the canvas was clicked on outside any Actor, and Ctrl was not pressed
            self.selectionModel().clearSelection()  # everything should be unselected
        self._canvas.draw()  # redraw the canvas with the updated selection

    def coc_add_rd(self, event):
        """
        Callback for the onClick event of the canvas when the "add Radio Dot" tool is selected.
        adds new RadioDot to the model given the coordinates of the click event
        :param event: event object
        """
        if event.xdata is not None and event.ydata is not None:
            # when the factory height was already set smaller than the standard z-pos of new Radio Dots,
            # the RD will be positioned at the ceiling of the factory.
            self.model().addItem(fg.RadioDot(event.xdata, event.ydata,
                                             min(float(self.model().get_factory().z),
                                                 float(self.standard_height_new_rds))))
        self._canvas.draw()

    def coc_add_ue(self, event):
        """
        Callback for the onClick event of the canvas when the "add User Endpoint" tool is selected.
        adds new UserEndpoint to the model given the coordinates of the click event. Stores current path loss according
        to applied path loss model in the UE data.
        :param event: event object
        """
        if event.xdata is not None and event.ydata is not None:
            # path loss
            # query all radio dots from the model
            radio_dot_indexes = self.model().indexOfType(fg.RadioDot)
            rd_list = list(map(self.model().data, radio_dot_indexes))

            # query all walls from the model
            wall_indexes = self.model().indexOfType(fg.Wall)
            wall_list = list(map(self.model().data, wall_indexes))

            # when the factory height was already set smaller than the standard z-pos of new User Entities,
            # the UE will be positioned at the ceiling of the factory.
            new_ue = fg.UserEndpoint(event.xdata, event.ydata,
                                     min(float(self.model().get_factory().z),
                                         float(self.standard_height_new_ues)), 60)  # req_pl default

            new_ue.achvPathloss = nodg.determine_pathloss(self.model().plmodel, rd_list, new_ue.xpos, new_ue.ypos,
                                                          new_ue.zpos, wall_list)
            self.model().addItem(new_ue)
        self._canvas.draw()

    def cop_rm_ac(self, event):
        """
        Callback for the onPick event of the canvas when the "remove Actor" tool is selected.
        removes actor from the model
        :param event: event object
        """
        for actorline in self._itemlines:  # go through all items and their associated plot lines
            if actorline[1] == event.artist:  # when the right actor was found
                self.model().removeItem(actorline[0])  # remove it
        self._canvas.draw()  # and redraw the canvas

    def coc_add_wall(self, event):
        """
        Callback for the onClick event of the canvas when the "add Wall" tool is selected.
        Initiates the drawing of a new wall.
        :param event: event object
        """

        # set the motion notify event handler
        com = self.com_add_wall
        self.canvas_onmotion = self._canvas.mpl_connect('motion_notify_event', com)

        self._wallStartPos = [event.xdata, event.ydata]  # point where the button was pressed
        self._heatmap.add_artist(self._tempWall)

    def cor_add_wall(self, _):
        """
        Callback for the onRelease event of the canvas when the "add Wall" tool is selected.
        Finishes the drawing of a new wall and adds the resulting wall to the model.
        """
        # unset the motion notify event handler
        self._canvas.mpl_disconnect(self.canvas_onmotion)

        wall_data_x, wall_data_y = self._tempWall.get_data()  # get final wall coordinates
        self._tempWall.remove()  # remove temporary wall artist object from the heatmap
        if len(wall_data_x) > 1:  # if a line was drawn (and not just a dot)
            factory_height = self.model().get_factory().z  # standard height of the wall: factory height
            # set standard material to "concrete" and thickness to 0.2
            self.model().addItem(fg.Wall(wall_data_x[0], wall_data_y[0], wall_data_x[1], wall_data_y[1], factory_height,
                                         fg.WallMaterial.CONCRETE, 0.2))
        self._tempWall.set_data(([-1], [-1]))  # reset tempwall

    def com_add_wall(self, event):
        """
        Callback for the motion notify event of the canvas when the "add Wall" tool is selected.
        If a wall is being drawn, updates canvas to reflect mouse motion. Otherwise, does nothing.
        :param event: event object
        """
        if event.xdata and event.ydata:  # don't draw walls when cursor is out of bounds
            if self._wallHorizontal:
                self._tempWall.set_data([self._wallStartPos[0], event.xdata],
                                        [self._wallStartPos[1], self._wallStartPos[1]])
            elif self._wallVertical:
                self._tempWall.set_data([self._wallStartPos[0], self._wallStartPos[0]],
                                        [self._wallStartPos[1], event.ydata])
            else:
                # hands-free wall drawing
                self._tempWall.set_data([self._wallStartPos[0], event.xdata], [self._wallStartPos[1], event.ydata])

        self._canvas.draw()

    def _redraw_labels(self, draw: bool = True) -> matplotlib.legend.Legend:
        """
        Draws the plot margins and legend according to the status of self._label_visibility
        :param draw: When False: sets all paramenters, but doesn't perform the final canvas re-rendering
        :return:
        """
        # border margins
        if self._label_visibility:
            self._heatmap.set_title("Network Model")
            self._heatmap.set_xlabel("[m]")
            self._heatmap.set_ylabel("[m]")
            self._colorbar.set_xticklabels([])
            self._colorbar.set_xticks([])
            self._colorbar.set_ylabel(self.window.cur_metric.name + " [" + self.window.cur_metric.unit + "]",
                                      loc='bottom')
            self._colorbar.yaxis.set_label_position('right')
            self._colorbar.yaxis.tick_left()

            margin_l = 0.07  # left
            margin_b = 0.08  # bottom
            margin_t = 0.10  # top
            margin_r = 0.03  # right
        else:
            self._heatmap.set_title("")
            self._heatmap.set_xlabel("")
            self._heatmap.set_ylabel("")
            self._colorbar.set_xlabel("")
            self._colorbar.set_ylabel("")

            margin_l = 0.07  # left
            margin_b = 0.05  # bottom
            margin_t = 0.03  # top
            margin_r = 0.03  # right
        mpl_plt.subplots_adjust(0 + margin_l, 0 + margin_b, 1 - margin_r, 1 - margin_t)

        # legend items
        ue_triangle = matplotlib.lines.Line2D([], [], color=self.uecolor, marker='^',
                                              linestyle="None", label='')
        rd_circle = matplotlib.lines.Line2D([], [], color=self.rdcolor, marker='o',
                                            linestyle="None", label='')

        # legend setup
        legend = self._heatmap.legend([ue_triangle, rd_circle], ['User Endpoint', 'Radio Dot'],
                                      bbox_to_anchor=(1.012, 1.01), loc='lower right')

        # legend visibility
        legend.set_visible(self._label_visibility)

        if draw:
            self._canvas.draw()
        return legend

    def set_labels(self, on: bool, draw: bool = True) -> None:
        """
        Sets the plot margins and legend according to the status of self._label_visibility
        Wrapper for _redraw_labels
        :param on: Turns legend on, adjusts margins
        :param draw: When False: sets all paramenters, but doesn't perform the final canvas re-rendering
        """
        if on:
            self._label_visibility = True  # redrawn in redrawView function
        else:
            self._label_visibility = False
        self._redraw_labels(draw)

    def plot_heatmap(self, pathloss_matrix: np.ndarray):
        """
        Plots the heatmap using the given path loss matrix.

        All pixels representing distances from RDs lower than 1m should be green.
        :param pathloss_matrix: pathloss matrix
        """

        # decide whether to draw in log scale (path loss) or linear
        draw_log = True if self.window.cur_metric == fg.metrics["pl"] else False

        # device whether higher values are green and low values red (SNR, power), or vice versa (path loss)
        more_is_better = False if self.window.cur_metric == fg.metrics["pl"] else True

        # determines the color coding of the colorbar
        # the final color is constructed using the levels of the three RGB components at a given position
        # path loss and SNR have different color curves (low path loss is green which is high SNR)
        cdict_greenToRed = {'red': ((0.0, 0.0, 0.0),  # at the top, there should be fn red
                                 (0.5, 1.0, 1.0),  # in the center, red should be at 100 %
                                 (1.0, 1.0, 1.0)),  # and at the bottom too

                         'green': ((0.0, 1.0, 1.0),  # with green, it's the other way around
                                   (0.5, 1.0, 1.0),
                                   (1.0, 0.0, 0.0)),

                         'blue': ((0.0, 0.0, 0.0),  # and we don't want blue at all
                                  (0.1, 0.0, 0.0),
                                  (1.0, 0.0, 0.0))
                         }

        cdict_RedToGreen = {'red': ((0.0, 1.0, 1.0),  # at the top, red should be at 100 %
                              (0.5, 1.0, 1.0),  # in the center, too
                              (1.0, 0.0, 0.0)),  # and at the bottom there should be fn red

                      'green': ((0.0, 0.0, 0.0),  # with green, it's the other way around
                                (0.5, 1.0, 1.0),
                                (1.0, 1.0, 1.0)),

                      'blue': ((0.0, 0.0, 0.0),  # and we don't want blue at all
                               (0.1, 0.0, 0.0),
                               (1.0, 0.0, 0.0))
                      }

        # construct the linear interpolation of colors in the colorbar using the information about top, center
        # and bottom from above
        cdict = cdict_RedToGreen if more_is_better else cdict_greenToRed

        gnrd = mpl_colors.LinearSegmentedColormap('GnRd', cdict)

        if np.all(np.isnan(pathloss_matrix)):
            # if the matrix is empty or NaN-only, reset the heatmap plot
            factory = self.model().get_factory()
            self._heatmap.set_xlim(0, factory.x)
            self._heatmap.set_ylim(0, factory.y)
            if self.window.isVisible():
                self.redrawView(reset=True)
        else:
            # else draw the heatmap using the pathloss matrix and a logarithmic scale
            # the min value for the scale and drawn values is the positive db value at 1 meter distance.
            # This will ensure that low db losses very close to the RDs don't impact the coloring
            self._colormesh = self._heatmap.pcolormesh(pathloss_matrix.transpose(), cmap=gnrd,
                                                       norm=(matplotlib.colors.LogNorm(vmin=self.minpl) if draw_log
                                                             else matplotlib.colors.Normalize()))

    def plot_colorbar(self, just_set=False):
        """
        Plots the colorbar at the left side of the plot
        :param just_set: set the scale labels without actually redrawing. Useful for the start of the application
        """
        # decide whether to draw in log scale (Path loss) or linear (power, SNR)
        draw_log = True if self.window.cur_metric == fg.metrics["pl"] else False

        if not just_set:
            self._colorbar.clear()
            mpl_plt.colorbar(self._colormesh, cax=self._colorbar)
        self._colorbar.set_yscale(('log' if draw_log else 'linear'))

    def set_tool_eventhandlers(self):
        """
        Sets the correct event handlers for the click and pick events of the plot according to currently selected tool.
        Also changes the mouse cursor accordingly.
        This function is called whenever the user changes the tool, after unset_tool_eventhandlers to unset the old
        tool.
        """
        tool = self.window.cur_tool
        cop = None  # the new on-pick event
        coc = None  # the new on-click event
        cor = None  # the new on-release event

        if tool == fg.Tool.MOUSE:
            cop = self.cop_mouse
            coc = self.coc_mouse
            self.setCursor(QtCore.Qt.CursorShape.ArrowCursor)  # sets the cursor for the Mouse tool to a - you may guess - mouse
        if tool == fg.Tool.ADD_RD:
            coc = self.coc_add_rd
            self.setCursor(QtCore.Qt.CursorShape.CrossCursor)  # sets the cursor for the Add RD tool to a cross
        if tool == fg.Tool.ADD_UE:
            coc = self.coc_add_ue
            self.setCursor(QtCore.Qt.CursorShape.CrossCursor)
        if tool == fg.Tool.ADD_WALL:
            coc = self.coc_add_wall
            cor = self.cor_add_wall
            # com_add_wall is set in coc_add_wall and unset in cor_add_wall
        if tool == fg.Tool.RM_AC:
            cop = self.cop_rm_ac
            self.setCursor(QtCore.Qt.CursorShape.CrossCursor)

        # and finally the event handlers are associated
        self.canvas_onpick = self._canvas.mpl_connect('pick_event', cop)
        self.canvas_onclick = self._canvas.mpl_connect('button_press_event', coc)
        self.canvas_onrelease = self._canvas.mpl_connect('button_release_event', cor)

    def unset_tool_eventhandlers(self):
        """
        Unselects all items in the selection mode, disconnects event handlers.
        This function is called whenever the user changes the tool, before set_tool_eventhandlers to set the new tool.
        """
        self._canvas.mpl_disconnect(self.canvas_onpick)
        self._canvas.mpl_disconnect(self.canvas_onclick)
        self._canvas.mpl_disconnect(self.canvas_onrelease)
        self.selectionModel().clearSelection()
        self._canvas.draw()

    def set_interactive_mode(self, newstate: bool) -> None:
        """
        turns the interactive mode on or off. Called when the user toggles the "Interactive calculation" toggle box.
        :param newstate: turns it on if true, else off
        """
        if self._interactivemode != newstate:
            self._interactivemode = newstate
            self._update_heatmap_visibility()
            self.redrawView()

    def set_bgimage_mode(self, newstate: bool) -> None:
        """
        turns the background image on or off. Called when the user toggles the "Show background image" toggle box.
        :param newstate: turns it on if true, else off
        """
        if self._bgimagemode != newstate:
            self._bgimagemode = newstate
            if newstate:
                self.plotBackgroundImage()
            else:
                self.redrawView(redraw_heatmap=False)
            self._canvas.draw()

    def _update_heatmap_visibility(self) -> None:
        """
        The heatmap consists both of the colored pixels in the plot, and the colorbar on the left.
        The visibility of both is updated here according to the value in self._interactivemode
        """
        self._colormesh.set_visible(self._interactivemode)
        self._colorbar.set_visible(self._interactivemode)
        self._canvas.draw()

    def set_ue_qoscolors(self, newstate: bool) -> None:
        """
        turns the red/green coloring of the UEs in the plot on or off. Called when the user toggles the
        "Show QoS constraints" toggle box.
        :param newstate: turns it on if true, else off
        """
        if self._layerqos != newstate:
            self._layerqos = newstate
            self._update_ue_qoscolors()

    def _update_ue_qoscolors(self) -> None:
        """
        The colorization of the UEs is updated for each UE.
        """
        for actor in self._itemlines:
            self.colorize_ue(actor)
        self._canvas.draw()

    def colorize_ue(self, ue: list) -> None:
        """
        Contains the logic to determine which color to colorize a UE to. But in the current software version, this
        functionality is not included, so it's commented out here.
        :param ue:
        """
        actor = ue[0].data()  # actorlines only contains the indexes. We extract the actual actor data from the model.
        if isinstance(actor, fg.UserEndpoint):
            if self._layerqos:
                if actor.achvPathloss <= actor.reqPathloss:
                    ue[1].set_color(self.uegreen)
                else:
                    ue[1].set_color(self.uered)
            else:
                ue[1].set_color(self.uecolor)


class Worker(QObject):
    """
    Class used to outsource the calculation of the path loss heatmap to a separate thread. This ensures responsiveness
    of the app and enables the user to abort calculation anytime.
    """
    # signals used to communicate between the main GUI thread and the worker thread i.e. calculation
    finished = pyqtSignal(np.ndarray)   # signals to the GUI that calculation process is completed
    # signals to the GUI that significant progress has been achieved, with the progress as parameter:
    progress = pyqtSignal(int)

    def __init__(self, plmodel, rd_list: list[fg.RadioDot], factory_x: float, factory_y: float,
                 heatmap_zpos: float, wall_list: list[fg.Wall], mutex: QtCore.QMutex):
        """

        :param plmodel: currently used path loss model or type PLModel (fn type hint here to avoid circular dependency)
        :param rd_list: list of radio dots in the factory
        :param factory_x: factory width (x)
        :param factory_y: factory length (y)
        :param heatmap_zpos: height (z) that should be represented by the heatmap pixels
        :param wall_list: list of walls in the factory
        :param mutex: mutex by the worker object which is used to protect access to self.stopped
        """
        super().__init__()
        self.plmodel, self.rd_list, self.factory_x, self.factory_y, self.heatmap_zpos, self.wall_list \
            = plmodel, rd_list, factory_x, factory_y, heatmap_zpos, wall_list
        self.stopped = False  # has the calculation been aborted by the user?
        self.mutex = mutex

    def run(self, ):
        """
        Entry point function which is executed when the thread is started
        """
        pathloss_matrix = nodg.calculate_pathloss_matrix(self.plmodel, self.rd_list, self.factory_x,
                                                         self.factory_y, self.heatmap_zpos, self.wall_list, self)
        self.finished.emit(pathloss_matrix)
