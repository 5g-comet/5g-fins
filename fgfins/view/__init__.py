# -----------------------------------------------------------
# Initialization and top-level layout of the main window.
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

from PyQt6.QtWidgets import *
from PyQt6 import QtCore
from PyQt6 import QtGui
import matplotlib.backends.backend_qtagg
from fgfins import model as fgm
from fgfins.view.matplotview import MatPlotView
from fgfins.view.sidebarviews import SidebarQosView, SidebarDeviceView
from fgfins.view.menubar import MainMenu
from fgfins.view.toolbar import ToolBar
import fgfins.datagenerator as fgdg
import fgfins as fg


class Window(QMainWindow):
    """
    Main class which controls the layout and behavior of the main window.
    """
    from .menubar_callbacks import closeEvent, open_file, save, saveas, export_plot, export_to_json,\
        heatmap_zpos_dialog, factorysize_dialog, plmodel_dialog, bgimage_dialog, about_dialog, snrparams_dialog

    def __init__(self):
        """
        - Initialization of all sorts of parameters which are shared between the functions of the class.
        - Instantiates one object of all the classes which are defined in other scripts that belong to "view"
        (e.g. one object of the MatPlotView class, one object of the MainMenu class etc.).
        - Instantiates model and selection model and applies them to the views
        - Reads out, which export file types are currently supported by MatPlotLib
        - Optional: Initiates the creation of random RDs and UEs to populate the factory at app start
        - Defines the outer-most layout of the window (left side goes the plot, right side go the boxes)
        - finally draws the main window
        """
        super(Window, self).__init__()

        self.setMinimumSize(800, 700)  # minimum window size. Window can only be shrinked up to those limits.
        self.resize(1050, 700)         # default window size at application start
        self.sw_title = '5G-FINS'
        self.setWindowTitle(self.sw_title)
        self._centralWidget = QWidget(flags=QtCore.Qt.WindowType.Widget)  # most outer widget that will contain all other widgets
        self.setCentralWidget(self._centralWidget)
        self._centralLayout = QHBoxLayout(self._centralWidget)  # horizontal layout. Left: heatmap, Right: sidebar

        # status bar, the bar at the bottom which shows additional tips and tooltips
        self.statusbar = self.statusBar()

        # menu bar
        _main_menu = MainMenu(self)  # view/menubar.py
        self.setMenuBar(_main_menu)

        self.cur_tool = None  # currently selected tool (set as enum fn.Tool option). Referenced by all view classes

        # how many randomly generated initial RDs and UEs should be drawn at software start?
        self.num_init_actors = 0  # none, in this software version

        # create the toolbar
        self.toolbar = ToolBar(self)
        self.addToolBar(self.toolbar)

        # savepath contains the path to the currently opened file
        self.cur_savepath = ""

        # has the user applied any changes that aren't saved to a file yet?
        self.changed_since_last_save = False

        # set the currently selected metric
        self.cur_metric = fg.metrics["pl"]

        # construct filter string for save plot dialog
        # this code reads out which file types are supported by MatPlotLib to export the plot to
        # By performing this calculation once at app start, the file types stay up-to-date when new ones are supported
        # in future versions of MatPlotLib
        self.supported_filetypes = \
            matplotlib.backends.backend_qtagg.FigureCanvasQTAgg.get_supported_filetypes_grouped()
        self.filters = list()
        for ft in self.supported_filetypes:
            prefixed_strings = ["*." + stem for stem in self.supported_filetypes[ft]]
            joined_pfx_strings = " ".join(prefixed_strings)
            filterstr = str(ft) + " (" + joined_pfx_strings + ")"
            self.filters.append(filterstr)
        # end of construct filter string block

        # Layer control
        self._metriccob = None      # Layer control: Metric picker combo box
        self._qoscb = None          # Layer control: Delay control box
        self.interactivecb = None   # Layer control: Interactive calculation control box

        # data model (model/view pattern)
        # The model is where all the data is stored.
        # See this introduction: https://doc.qt.io/qt-5/model-view-programming.html
        self.model = fgm.ActorModel()
        self.model.dataChanged.connect(self.onDataChanged)  # connect view callback function for when data was changed

        # set the default path-loss model
        self.model.plmodel = fgdg.FSPLModel(3.7 * 1e9)  # FSPL model with a frequency of 3.7 GHz
        self.model.plmodel_changed.connect(self.onDataChanged)
        self.model.snrparams_changed.connect(self.onDataChanged)

        # set view for the matplotlib canvas
        self.canvas = MatPlotView(self)
        self.canvas.setParent(self)
        self.canvas.setFrameShape(QStyleOptionFrame().frameShape.NoFrame)
        self.canvas.setModel(self.model)  # all views should use the same data model
        self.canvas.init_after_setmodel()

        # set the sidebar views
        self._qosView = SidebarQosView(self)
        self._qosView.setParent(self)
        self._qosView.setFrameShape(QStyleOptionFrame().frameShape.NoFrame)
        self._qosView.setModel(self.model)
        self._qosView.init_after_model()

        self._deviceView = SidebarDeviceView(self)
        self._deviceView.setParent(self)
        self._deviceView.setModel(self.model)
        self._deviceView.init_factorysize_validators()

        # set common selection model for all views
        # The selection model keeps track of which model items (Actors in this case) were selected by the users.
        # All views can then mark the selected actors, or show relevant information about them.
        # Selection models are part of the Qt Model/View Programming pattern
        # See this introduction: https://doc.qt.io/qt-5/model-view-programming.html#handling-selections-in-item-views
        self._selModel = QtCore.QItemSelectionModel(self.model)
        self.canvas.setSelectionModel(self._selModel)
        self._qosView.setSelectionModel(self._selModel)
        self._deviceView.setSelectionModel(self._selModel)

        # reset selections, fn items should be selected at start
        self._selModel.clearSelection()

        # Toolbar: Mouse tool should be selected at application start
        self.toolbar.mouse_tb.toggle()

        # If the legend button is active at app start (which it is in this version), display legend and labels in plot.
        self.canvas.set_labels(self.toolbar.isLegendTurnedOn(), draw=False)

        # Add the MatPlotView to the main window layout, giving it 2/3 of the available space
        self._centralLayout.addWidget(self.canvas, stretch=2)

        # Create and add the views and layer control box at the right-hand side, giving it 1/3 of the available space
        self.create_sidebar()

        # set initial random data (if applicable)
        if self.num_init_actors:
            # read out factory dimensions
            factory = self.model.get_factory()
            # create random Radio Dots
            rd_list = fgdg.create_rds_at_random(self.num_init_actors, factory.x, factory.y, factory.z)
            self.model.addItems(rd_list)

            # create random User Entities
            ue_list = fgdg.create_ues_at_random(self.num_init_actors, factory.x, factory.y, factory.z)
            self.model.addItems(ue_list)

        # set Window spawn point to have it spawn in screen center
        framegm = self.frameGeometry()
        centerpoint = QtGui.QGuiApplication.primaryScreen().geometry().center()
        framegm.moveCenter(centerpoint)
        self.move(framegm.topLeft())

        # and finally show the window
        self.show()

    def keyPressEvent(self, a0: QtGui.QKeyEvent) -> None:
        """
        Callback method that gets called when the Ctrl-key is pressed and fn widget is in focus.
        Forwards all Ctrl-key presses to the MatPlotView, so it can activate its multi-select feature.
        :param a0: event reference
        """
        if a0.key() == QtCore.Qt.Key.Key_Control:
            QtGui.QGuiApplication.sendEvent(self.canvas, a0)

    def keyReleaseEvent(self, a0: QtGui.QKeyEvent) -> None:
        """
        Callback method that gets called when the Ctrl-key is released and fn widget is in focus.
        Forwards all Ctrl-key releases to the MatPlotView, so it can deactivate its multi-select feature.
        :param a0: event reference
        """
        if a0.key() == QtCore.Qt.Key.Key_Control:
            QtGui.QGuiApplication.sendEvent(self.canvas, a0)

    def onDataChanged(self):
        """
        Callback method that gets called whenever the data in the model changes. Sets the "changed-since-last-save* flag
        to indicate that the latest changes were not saved to a file yet, and updates the window title to add a "*"
        behind the file name, indicating the unsaved changes to the user.
        Please note that MatPlotView, SidebarDeviceView and SidebarQosView each have their own event handlers for this
        event (dataChanged). Each of them handles the data change on their own, e.g. MatPlotView redraws the whole
        canvas to reflect the changes. Look at the classes to find them.
        """
        self.changed_since_last_save = True
        self.updateWindowTitle()

    def updateWindowTitle(self):
        """
        Performs the change of the Window title to include the path of the currently loaded file and the "*" to indicate
        unsaved changes, if applicable.
        """
        wt = self.sw_title
        if self.cur_savepath:
            wt = wt + " - " + self.cur_savepath
        if self.changed_since_last_save:
            wt = wt + " *"
        self.setWindowTitle(wt)

    def create_sidebar(self):
        """
        Contains the layout of the right-hand sidebar which encompasses the SidebarDeviceView, SidebarQoSView and the
        layer control box. The two views are defined in their separate file (sidebarviews.py) and just placed here. The
        conrol box however is completely defined here with its layout and logic.
        """
        # outerbox == surrounding vertical grouping widget for the whole sidebar
        outerbox = QWidget(self._centralWidget, flags=QtCore.Qt.WindowType.Widget)
        self._centralLayout.addWidget(outerbox, alignment=QtCore.Qt.AlignmentFlag.AlignVCenter, stretch=1)
        outerbox.setMaximumWidth(350)
        outerlay = QVBoxLayout(outerbox)

        # Add the two sidebar views
        outerlay.addWidget(self._deviceView, alignment=QtCore.Qt.AlignmentFlag.AlignTop)
        outerlay.addWidget(self._qosView, alignment=QtCore.Qt.AlignmentFlag.AlignTop)

        # Add the layer control box
        lcbox = QGroupBox("Layer control", outerbox)
        outerlay.addWidget(lcbox, alignment=QtCore.Qt.AlignmentFlag.AlignTop)

        # layercontrolbox layout and association of event-handlers
        lclay = QVBoxLayout(lcbox)

        # Metric picker combo box with label
        # outer horizontal layout to encompass label and box
        metricbox = QWidget(lcbox, flags=QtCore.Qt.WindowType.Widget)
        metricboxlay = QHBoxLayout(metricbox)

        metriclabel = QLabel(metricbox)
        metriclabel.setText("metric: ")
        metricboxlay.addWidget(metriclabel, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)

        # fill the combo box with content
        self._metriccob = QComboBox(lcbox)
        for metric in fg.metrics.values():
            self._metriccob.addItem(metric.name)
        metricboxlay.addWidget(self._metriccob, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)

        # add to overall layer control box
        metricboxlay.addStretch(1)
        lclay.addWidget(metricbox)
        self._metriccob.currentIndexChanged.connect(self.metriccob_onindexchanged)

        # QoS checkbox
        self._qoscb = QCheckBox("Show QoS constraints")
        self._qoscb.setCheckState(QtCore.Qt.CheckState.Checked)
        lclay.addWidget(self._qoscb, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)   # add checkbox for delay
        self._qoscb.stateChanged.connect(self.qosbox_onclick)        # associate click event handler

        # Interactive calculation checkbox
        self.interactivecb = QCheckBox("Interactive calculation")
        self.interactivecb.setCheckState(QtCore.Qt.CheckState.Checked)
        lclay.addWidget(self.interactivecb, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)  # add box for interactive mode
        self.interactivecb.stateChanged.connect(self.interactivemodebox_onclick)  # associate click event handler

        # background image checkbox
        self.bgimagecb = QCheckBox("Show background image")
        self.bgimagecb.setCheckState(QtCore.Qt.CheckState.Checked)
        lclay.addWidget(self.bgimagecb, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)  # add box for background image
        self.bgimagecb.stateChanged.connect(self.bgimagebox_onclick)  # associate click event handler

        # set canvas to the right defaults indicated by the checkbox states
        self.canvas.set_interactive_mode(self.interactivecb.checkState() == QtCore.Qt.CheckState.Checked)
        self.canvas.set_ue_qoscolors(self._qoscb.checkState() == QtCore.Qt.CheckState.Checked)

    def metriccob_onindexchanged(self, index):
        """
        Callback method that gets called whenever the plot metric is changed using the combobox.
        Sets the metric and issues a redrawing of the plot canvas.
        """
        self.cur_metric = list(fg.metrics.values())[index]
        self.canvas.redrawHeatmap()

    def interactivemodebox_onclick(self, _):
        """
        Callback method that gets called whenever the "interactive" checkbox of the layer control box is clicked on.
        Passes the information on to the MatPlotView to process.
        """
        self.canvas.set_interactive_mode(self.interactivecb.checkState() == QtCore.Qt.CheckState.Checked)

    def bgimagebox_onclick(self, _):
        """
        Callback method that gets called whenever the "Show background image" checkbox of the layer control box is
        clicked on.
        Passes the information on to the MatPlotView to process.
        """
        self.canvas.set_bgimage_mode(self.bgimagecb.checkState() == QtCore.Qt.CheckState.Checked)

    def qosbox_onclick(self, _):
        """
        Callback method that gets called whenever the "QoS constraints" checkbox of the layer control box is clicked on.
        Passes the information on to the MatPlotView to show or hide UE coloring.
        """
        self.canvas.set_ue_qoscolors(self._qoscb.checkState() == QtCore.Qt.CheckState.Checked)
