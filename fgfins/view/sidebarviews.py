# -----------------------------------------------------------
# contains the code related to the information boxes on the right-hand side.
# Including: Layout, labels, event handlers
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

import abc
import fgfins as fg
from PyQt6.QtWidgets import *
from PyQt6 import QtCore
from PyQt6 import QtGui
import math
import typing


class SidebarMeta(type(QAbstractItemView), type(abc.ABC)):
    """
    Metaclass that allows to define SidebarView as an abstract class while also inheriting from QAbstractItemView.
    No more and fn less.
    """
    pass


class SidebarView(QAbstractItemView, abc.ABC, metaclass=SidebarMeta):
    """
    Abstract class combining the common functionality of SidebarDeviceView and SidebarQosView.
    The common layout of both is defined here.
    """
    def __init__(self, window):
        """
        Contains the common layouting code for both SidebarDeviceView and SidebarQosView
        :param window:
        """
        super(SidebarView, self).__init__()
        self.window = window

        class QNoTabTableWidget(QTableWidget):
            """
            Customization of the QTableWidget class to render the tab-key functionality (tabbing through the cells)
            disfunctional.
            """
            def __init__(self, *args):
                super(QNoTabTableWidget, self).__init__(*args)
                self.setFrameShape(QStyleOptionFrame().frameShape.NoFrame)

            def keyPressEvent(self, a0: QtGui.QKeyEvent) -> None:
                if a0.key() == QtCore.Qt.Key.Key_Tab:
                    pass

        # set comma separator to '.' to ensure that validators accept it
        self.locale = QtCore.QLocale(QtCore.QLocale.Language.English, QtCore.QLocale.Country.UnitedStates)

        # flag to indicate that the user changed an edit field's data prior to exiting focus
        self.userChangedProperties = False

        # basic layouting
        self.setStyleSheet("background-color: rgba(0,0,0,0)")
        self.setFrameShape(QStyleOptionFrame().frameShape.NoFrame)
        self.setMaximumHeight(230)
        _outerLayout = QHBoxLayout(self)
        _outerLayout.setContentsMargins(0, 0, 0, 0)
        _outerLayout.setSpacing(0)

        self.table = QNoTabTableWidget(4, 2, self)
        groupbox = QGroupBox(self.label, self)  # creating the groupbox that frames the view visually
        innerLayout = QVBoxLayout(groupbox)
        innerLayout.setContentsMargins(0, 0, 0, 0)
        innerLayout.setSpacing(0)

        _outerLayout.addWidget(groupbox)
        innerLayout.addWidget(self.table)
        self.table.setStyleSheet("""
        QHeaderView::section {
            background-color: Gainsboro
            }
        
        QTableCornerButton::section {
            background-color: Gainsboro
            }
        
        QLineEdit {
            background-color: white;
            }
            
        QTableWidget::item {
            padding: 0 10
            }
        
        """)
        self.table.horizontalHeader().hide()
        self.table.verticalHeader().hide()
        self.table.setShowGrid(False)
        self.table.horizontalHeader().setStretchLastSection(True)
        self.table.setColumnWidth(0, 150)

        self.cell = list()  # two-dimensional list that will contain the contents of the table cells for reference

    @property
    @abc.abstractmethod
    def label(self):
        # Each view - SidebarDeviceView and SidebarQosView - has its own visible label.
        # Hence, this abstract class only contains the constraint that its subclasses must provide a label.
        pass

    def verticalOffset(self) -> int:
        return 0

    def horizontalOffset(self) -> int:
        return 0

    def visualRegionForSelection(self, selection: QtCore.QItemSelection) -> QtGui.QRegion:
        # not actually used, just to hide a "not implemented" exception
        return QtGui.QRegion()

    def moveCursor(self, QAbstractItemView_CursorAction, Union,
                   Qt_KeyboardModifiers=None, Qt_KeyboardModifier=None):
        # not actually used, just to hide a "not implemented" exception
        return QtCore.QModelIndex()

    def indexAt(self, p: QtCore.QPoint) -> QtCore.QModelIndex:
        # not actually used, just to hide a "not implemented" exception
        return QtCore.QModelIndex()

    def visualRect(self, index: QtCore.QModelIndex) -> QtCore.QRect:
        # not actually used, just to hide a "not implemented" exception
        return QtCore.QRect()

    def dataChanged(self, topLeft: QtCore.QModelIndex, bottomRight: QtCore.QModelIndex,
                    roles: typing.Iterable[int] = ..., **kwargs):
        #  this is overritten in SidebarDeviceView and SidebarQosView. Look there!
        pass

    def onEditFieldStringChanged(self) -> None:
        """
        Function which sets a flag to indicate that the user altered a number in an edit field.
        This way, when the user leaves the focus of the edit field, the set data can be inserted via
        self.onEditFieldFocusLeave.

        Also replaces all "," input chars by "." to have the input comply to the double validator (which is set to US
        English)
        """
        sender = QtCore.QObject.sender(self)
        if isinstance(sender, QLineEdit):
            sender.setText(sender.text().replace(",", "."))

        self.userChangedProperties = True


class SidebarDeviceView(SidebarView):
    """
    Contains all the code for the Device information box in the right-hand sidebar.
    """
    label = "Device"

    def __init__(self, window):
        """
        Sets the layout and initial table contents
        :param window: window window
        """
        super(SidebarDeviceView, self).__init__(window)
        cur_row = -1  # current row

        #  --- 1st row: category / distance
        cur_row = cur_row + 1
        self.cell.append([QTableWidgetItem("category + ID"), QTableWidgetItem("n/a")])
        self.table.setItem(cur_row, 0, self.cell[cur_row][0])
        self.table.setItem(cur_row, 1, self.cell[cur_row][1])

        #  --- 2nd row: actor coordinates / wall starting point
        cur_row = cur_row + 1

        # coordinates input LineEdits and group
        line2_wrapper_widget = QWidget()
        _innerLayout = QHBoxLayout(line2_wrapper_widget)
        _innerLayout.setContentsMargins(0, 0, 0, 0)
        _innerLayout.setSpacing(0)

        self.data_coord_x = QLineEdit("0", line2_wrapper_widget)
        # callback when finished entering a new coordinate
        self.data_coord_x.textEdited.connect(self.onEditFieldStringChanged)
        self.data_coord_x.editingFinished.connect(self.onEditFieldFocusLeave)

        self.data_coord_y = QLineEdit("0", line2_wrapper_widget)
        self.data_coord_y.textEdited.connect(self.onEditFieldStringChanged)
        self.data_coord_y.editingFinished.connect(self.onEditFieldFocusLeave)

        self.data_coord_z = QLineEdit("0", line2_wrapper_widget)
        self.data_coord_z.textEdited.connect(self.onEditFieldStringChanged)
        self.data_coord_z.editingFinished.connect(self.onEditFieldFocusLeave)

        _innerLayout.addWidget(self.data_coord_x, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
        _innerLayout.addWidget(self.data_coord_y, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
        _innerLayout.addWidget(self.data_coord_z, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)

        self.cell.append([QTableWidgetItem("position (x, y, z)"), line2_wrapper_widget])  # overridden later
        self.table.setItem(cur_row, 0, self.cell[cur_row][0])
        self.table.setCellWidget(cur_row, 1, self.cell[cur_row][1])

        self.factory_xsize_val = QtGui.QDoubleValidator(0, math.inf, 4)  # overridden later
        self.factory_ysize_val = QtGui.QDoubleValidator(0, math.inf, 4)
        self.factory_zsize_val = QtGui.QDoubleValidator(0, math.inf, 4)

        #  --- 3rd row: wall end point
        cur_row = cur_row + 1

        # wall end point input LineEdits and group
        line3_wrapper_widget = QWidget()
        _innerLayout = QHBoxLayout(line3_wrapper_widget)
        _innerLayout.setContentsMargins(0, 0, 0, 0)
        _innerLayout.setSpacing(0)

        self.endpoint_x = QLineEdit("0", line3_wrapper_widget)
        # callback when finished entering a new coordinate
        self.endpoint_x.textEdited.connect(self.onEditFieldStringChanged)
        self.endpoint_x.editingFinished.connect(self.onEditFieldFocusLeave)

        self.endpoint_y = QLineEdit("0", line3_wrapper_widget)
        self.endpoint_y.textEdited.connect(self.onEditFieldStringChanged)
        self.endpoint_y.editingFinished.connect(self.onEditFieldFocusLeave)

        self.endpoint_x_val = QtGui.QDoubleValidator(0, math.inf, 4)  # overriden later
        self.endpoint_y_val = QtGui.QDoubleValidator(0, math.inf, 4)

        _innerLayout.addWidget(self.endpoint_x, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
        _innerLayout.addWidget(self.endpoint_y, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)

        self.cell.append([QTableWidgetItem("end point (x, y) + height"), line3_wrapper_widget])  # overridden later
        self.table.setItem(cur_row, 0, self.cell[cur_row][0])
        self.table.setCellWidget(cur_row, 1, self.cell[cur_row][1])

        #  --- 4th row: wall material and thickness
        cur_row = cur_row + 1

        line4_wrapper_widget = QWidget()
        _innerLayout = QHBoxLayout(line4_wrapper_widget)
        _innerLayout.setContentsMargins(0, 0, 0, 0)
        _innerLayout.setSpacing(0)

        self.wall_material_select = QComboBox(line4_wrapper_widget)
        self.wall_material_select.setStyleSheet("""
        QComboBox {
            background: white;
            color: black;
        }
        
        QComboBox::drop-down {
            border-left-width: 1px;
            border-left-color: darkgray;
            border-left-style: solid; /* just a single line */
        }
        
        QComboBox::down-arrow {
            image: url(assets/arrowDown.svg);
        }
        """)
        self.wall_material_select.insertItems(0, [e.name.lower() for e in fg.WallMaterial])
        self.wall_material_select.setProperty("old_value", self.wall_material_select.currentIndex())
        self.wall_material_select.activated.connect(self.onWallMaterialSelected)

        self.wall_thickness = QLineEdit("0.2", line4_wrapper_widget)
        self.wall_thickness.textEdited.connect(self.onEditFieldStringChanged)
        self.wall_thickness.editingFinished.connect(self.onEditFieldFocusLeave)

        _innerLayout.addWidget(self.wall_material_select, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
        _innerLayout.addWidget(self.wall_thickness, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)

        self.cell.append([QTableWidgetItem("material + thickness"), line4_wrapper_widget])  # overridden later
        self.table.setItem(cur_row, 0, self.cell[cur_row][0])
        self.table.setCellWidget(cur_row, 1, self.cell[cur_row][1])

        self.wall_thickness_val = QtGui.QDoubleValidator(0.01, math.inf, 4)
        self.wall_thickness_val.setLocale(self.locale)
        self.wall_thickness.setValidator(self.wall_thickness_val)

    # visibility
        self.table.setVisible(False)

    def init_factorysize_validators(self):
        """
        Some additional initialization steps that can only be completed after the data model has been connected to the
        SidebarDeviceView (which happens in the script that creates the object). So the calling order is:
        1. create SidebarDeviceView object, 2. connect data model to object, 3. call this function
        This function sets the validator of the input fields to the size of the factory.
        Validators are objects that ensure that fn invalid inputs are accepted by the input fields. Every attempt to set
        a coordinate to a value higher than the factory size will be rejected.
        """
        factory = self.model().get_factory()  # get the factory size
        self.factory_xsize_val = QtGui.QDoubleValidator(0, factory.x, 4)  # create the validators and save for reference
        self.factory_ysize_val = QtGui.QDoubleValidator(0, factory.y, 4)
        self.factory_zsize_val = QtGui.QDoubleValidator(0, self.model().get_factory().z, 4)
        self.endpoint_x_val = QtGui.QDoubleValidator(0, factory.x, 4)
        self.endpoint_y_val = QtGui.QDoubleValidator(0, factory.y, 4)

        self.factory_xsize_val.setLocale(self.locale)
        self.factory_ysize_val.setLocale(self.locale)
        self.factory_zsize_val.setLocale(self.locale)
        self.endpoint_x_val.setLocale(self.locale)
        self.endpoint_y_val.setLocale(self.locale)

        self.data_coord_x.setValidator(self.factory_xsize_val)  # associate the validators to the input fields
        self.data_coord_y.setValidator(self.factory_ysize_val)
        self.data_coord_z.setValidator(self.factory_zsize_val)
        self.endpoint_x.setValidator(self.endpoint_x_val)
        self.endpoint_y.setValidator(self.endpoint_y_val)

        # set the callback function that should be executed whenever the factory size changes. It will update the
        # validators to the new factory size
        self.model().factory_size_changed.connect(self.onFactorySizeChanged)

    def onWallMaterialSelected(self):
        self.userChangedProperties = True
        self.onEditFieldFocusLeave()

    def onFactorySizeChanged(self):
        """
        Callback function that is executed whenever the factory size changes in the model. Updates the validators for
        the input fields (to ensure that fn coordinates larger than the factory size can be accepted as input).
        """
        self.factory_xsize_val.setTop(self.model().get_factory().x)  # the new highest accepted value is the size
        self.factory_ysize_val.setTop(self.model().get_factory().y)  # in all dimensions
        self.factory_zsize_val.setTop(self.model().get_factory().z)

    def selectionChanged(self, _, __) -> None:
        """
        Called by Qt Model/View message system each time the selection in the associated selection model changes.
        When only one actor is selected, will show information about it.
        When two actors are selected at the same time, will calculate and show the distance between them.
        See https://doc.qt.io/qt-5/qitemselectionmodel.html#selectionChanged
        """
        selection = self.selectionModel().selection().indexes()  # get the indexes of all selected items
        if len(selection) == 1:
            # show information about the selected actor

            self.table.setVisible(True)

            # actor is given as a list of the following format:
            # [type (RD/UE/Wall), id number, x position, y position, z position]
            # walls are given as:
            # [type (RD/UE/Wall), startx, starty, endx, endy, height, material, thickness]
            item = selection[0].data(fg.Roles.DeviceRole)  # extract the data corresponding to the index from the model

            if issubclass(item[0], fg.Actor):
                # category + ID section
                self.cell[0][0].setText('category + ID')
                if item[0] == fg.RadioDot:
                    self.cell[0][1].setText("Radio Dot #" + str(item[1]))  # "Radio Dot #{ID}"
                elif item[0] == fg.UserEndpoint:
                    self.cell[0][1].setText("User Endpoint #" + str(item[1]))  # "User Endpoint #{ID}"

                # coordinates section
                self.data_coord_x.setText(f'{item[2]:.4f}')  # set text to x-position, truncating the number
                self.data_coord_y.setText(f'{item[3]:.4f}')  # set text to y-position, truncating the number
                self.data_coord_z.setText(f'{item[4]:.4f}')  # set text to z-position, truncating the number

                self.cell[1][0].setText("position (x, y, z)")

                self.table.showRow(1)
                self.table.hideRow(2)
                self.table.hideRow(3)

            elif item[0] == fg.Wall:
                self.cell[0][0].setText('category')
                self.cell[0][1].setText("Wall")

                # startpoint section
                self.cell[1][0].setText("start point (x, y) + height")
                self.data_coord_x.setText(f'{item[1]:.4f}')  # startx
                self.data_coord_y.setText(f'{item[2]:.4f}')  # starty
                self.data_coord_z.setText(f'{item[5]:.4f}')  # height

                self.cell[2][0].setText("end point (x,y)")
                self.endpoint_x.setText(f'{item[3]:.4f}')  # endx
                self.endpoint_y.setText(f'{item[4]:.4f}')  # endy

                self.cell[3][0].setText("material + thickness")
                self.wall_material_select.setCurrentIndex(item[6])
                self.wall_thickness.setText(f'{item[7]:.4f}')

                self.table.showRow(1)
                self.table.showRow(2)
                self.table.showRow(3)

        elif len(selection) == 2:
            # two items selected. If they are actors, calculate and show their distance
            self.table.setVisible(True)

            # actors are given as a list of the following format:
            # [type (Radio Dot or User Entity), id number, x position, y position, z position]
            actor1 = selection[0].data(fg.Roles.DeviceRole)
            actor2 = selection[1].data(fg.Roles.DeviceRole)

            if issubclass(actor1[0], fg.Actor) and issubclass(actor2[0], fg.Actor):
                # calculate Euclidean distance between the actors
                deltax = math.fabs(actor1[2] - actor2[2])
                deltay = math.fabs(actor1[3] - actor2[3])
                distance = math.sqrt(deltax ** 2 + deltay ** 2)

                # and set text accordingly
                self.cell[0][0].setText('distance')
                self.cell[0][1].setText(f'{distance:.4f}')  # truncating the distance number
                self.table.hideRow(1)  # the other rows is not used and hidden
                self.table.hideRow(2)
                self.table.hideRow(3)
            else:
                self.table.setVisible(False)
        else:
            # more than two items were selected, don't show anything
            self.table.setVisible(False)

    def onEditFieldFocusLeave(self) -> None:
        """
        Function which is called whenever a user leaves the focus of an edit field or combobox.
        First checks whether data was really changed. If so: Reads out the value given by the user and initiates the
        data change in the model.
        """
        if self.userChangedProperties:
            self.userChangedProperties = False
            selection = self.selectionModel().selection().indexes()  # read out indexes of selected items
            if len(selection) == 1:
                # only perform this when one item was selected
                item = selection[0].data()  # read out the data of the item from the model, given the index
                if isinstance(item, fg.Actor):
                    # alter actor coordinates. Replace entered "," by "." (unfortunately, this is not handled
                    # automatically by the validator and has to be caught here)
                    item.xpos = float(self.data_coord_x.text())
                    item.ypos = float(self.data_coord_y.text())
                    item.zpos = float(self.data_coord_z.text())
                if isinstance(item, fg.Wall):
                    # alter the start and end points and height.
                    item.startx = float(self.data_coord_x.text())
                    item.starty = float(self.data_coord_y.text())
                    item.endx = float(self.endpoint_x.text())
                    item.endy = float(self.endpoint_y.text())
                    newheight = float(self.data_coord_z.text())
                    if newheight <= self.model().get_factory().z:  # wall heights shall not exceed factory height
                        item.height = float(self.data_coord_z.text())
                    item.material = self.wall_material_select.currentIndex()
                    item.thickness = float(self.wall_thickness.text())
                self.model().setData(selection[0], item)  # push the updated item into the model

    def dataChanged(self, topLeft: QtCore.QModelIndex, bottomRight: QtCore.QModelIndex,
                    roles: typing.Iterable[int] = ..., **kwargs) -> None:
        """
        Called by Qt Model/View message system each time data in the associated model changes.
        When the user has changed actor coordinates with an edit field, onEditFieldFocusLeave() is called and
        initiated the data change in the model. After that, this method here is called and ensures that the displayed
        information is updated once again to reflect the data model. Foremost, numbers entered by the user are
        truncated by the rules defined in selectionChanged()
        See https://doc.qt.io/qt-5/qabstractitemmodel.html#dataChanged
        :param **kwargs:
        :param topLeft:     First index impacted by the change
        :param bottomRight: Last index impacted by the change
        :param roles:       To specify which data roles have been motified
        """
        self.selectionChanged(None, None)


class SidebarQosView(SidebarView):
    """
    Contains all the code for the QoS information box in the right-hand sidebar.
    """
    label = "QoS"

    def __init__(self, window):
        """
        Sets the layout and initial table contents
        :param window: window window
        """
        super(SidebarQosView, self).__init__(window)

        cur_row = -1  # current row

        #  --- 1st row: Path Loss
        cur_row = cur_row + 1

        # path loss ach. label + req. lineedit + group
        wrapper_widget = QWidget()
        _innerLayout = QHBoxLayout(wrapper_widget)
        _innerLayout.setContentsMargins(0, 0, 0, 0)
        _innerLayout.setSpacing(0)

        double_val = QtGui.QDoubleValidator(0, float("inf"), 4)
        double_val.setLocale(self.locale)

        self.pathloss_achv = QLabel("0")

        self.pathloss_req = QLineEdit("0", wrapper_widget)
        self.pathloss_req.setValidator(double_val)

        # callback when finished entering a new required path loss
        self.pathloss_req.textEdited.connect(self.onEditFieldStringChanged)
        self.pathloss_req.editingFinished.connect(self.onEditFieldFocusLeave)

        _innerLayout.addWidget(self.pathloss_achv, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)
        _innerLayout.addWidget(self.pathloss_req, alignment=QtCore.Qt.AlignmentFlag.AlignLeft)

        self.cell.append([QTableWidgetItem("Path Loss (achv, limit)"), wrapper_widget])
        self.table.setItem(cur_row, 0, self.cell[cur_row][0])
        self.table.setCellWidget(cur_row, 1, self.cell[cur_row][1])

        self.table.hideRow(1)
        self.table.hideRow(2)
        self.table.hideRow(3)
        self.setMaximumHeight(130)

        # visibility
        self.table.setVisible(False)

    def init_after_model(self):
        """
        Initialization steps that can only be conducted after the model has been set.
        """
        self.model().plmodel_changed.connect(self.plmodelChanged)

    def plmodelChanged(self):
        """
        Called by Qt Model/View message system each time the path loss model changes. Ensures that the displayed
        achieved path loss is updated to reflect the data model.
        """
        self.selectionChanged(None, None)

    def selectionChanged(self, _, __) -> None:
        """
        Called by Qt Model/View message system each time the selection in the associated selection model changes.
        When only one actor is selected, will calculate and show its achieved path loss. Also shows the required PL.
        Otherwise, nothing is shown.
        See https://doc.qt.io/qt-5/qitemselectionmodel.html#selectionChanged
        """
        selection = self.selectionModel().selection().indexes()  # get the indexes of all selected items
        if len(selection) == 1:
            # only one actor selected
            # actor is given as a list of the following format:
            # [x position, y position, z position, path loss requirement set for the actor]
            # if the index did not belong to an existing actor, the list is empty
            actor = selection[0].data(fg.Roles.QoSRole)  # model returns [] if queried item is not a UE

            if actor:
                # index does belong to an existing User Entity
                self.table.setVisible(True)

                # calculate the pathloss with the given Radio Dots, Walls and pathloss model
                pltext = f'{actor[4]:.4f}'

                self.pathloss_achv.setText(pltext)  # set achieved path loss text
                self.pathloss_req.setText(f'{actor[3]:.4f}')  # set required path loss text

                return
        self.table.setVisible(False)  # if more than one actor was selected, don't show anything

    def onEditFieldFocusLeave(self) -> None:
        """
        Function which is called whenever a user changes the required path loss of an actor with the provided edit
        field. Reads out the value given by the user and initiates the data change in the model.
        """
        if self.userChangedProperties:
            self.userChangedProperties = False
            selection = self.selectionModel().selection().indexes()  # read out indexes of selected actors
            if len(selection) == 1:
                # only perform this when one actor was selected
                actor = selection[0].data()  # read out the data of the actor from the model, given the index
                if isinstance(actor, fg.UserEndpoint):  # if the actor is a UE
                    actor.reqPathloss = float(self.pathloss_req.text())  # set the reqPathloss to the entered value
                    self.model().setData(selection[0], actor)  # push the updated actor into the model

    def dataChanged(self, topLeft: QtCore.QModelIndex, bottomRight: QtCore.QModelIndex,
                    roles: typing.Iterable[int] = ..., **kwargs) -> None:
        """
        Called by Qt Model/View message system each time data in the associated model changes.
        When the user has changed actor reqPathloss with an edit field, onEditFieldFocusLeave() is called and
        initiated the data change in the model. After that, this method here is called and ensures that the displayed
        information is updated once again to reflect the data model. Foremost, numbers entered by the user are
        truncated by the rules defined in selectionChanged()
        See https://doc.qt.io/qt-5/qabstractitemmodel.html#dataChanged
        :param **kwargs:
        :param topLeft:     First index impacted by the change
        :param bottomRight: Last index impacted by the change
        :param roles:       To specify which data roles have been motified
        """
        self.selectionChanged(None, None)
