# 5G-FINS

## Structure of the view
The view contains code regarding the layout and functionality of the main window and everything inside. The main menu and toolbar. The plotting environment and the code that determines how data is visualized. The sidebar views and layer control box. It is structered as follows:

- `__init__.py` - Initialization and top-level layout of the main window. The glue that holds everything together. Also contains the code for the layer control box. Executed automatically when the package is loaded.
- `matplotview.py` - the plotting surface and everything about it. How the data is visualized. What happens when the plot is clicked on.
- `menubar.py` - main menu layout
- `menubar_callbacks.py` - What happens when an entry in a menu is clicked on. Contains layout and functionality of all dialog windows
- `sidebarviews.py` - layout and functionality of the Device and QoS sections of the sidebar. Note that the layer control box is handled by `__init__.py` as it is not technically a view that shows data.
- `toolbar.py` - layout and logic of the toolbar. What happens when a toolbar button is clicked on. Note that the behavior of the plotting surface (depending on the selected tool) is defined in `matplotview.py`. This file only contains information about what tools exist and how to switch between them, not the behavior of the views themselves.
