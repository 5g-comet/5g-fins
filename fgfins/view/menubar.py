# -----------------------------------------------------------
# contains the code which constructs the menu bar and sets callback functions
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

from PyQt6.QtWidgets import *
from PyQt6.QtGui import QAction


class MainMenu(QMenuBar):
    """
    Contains the code which constructs the menu bar and sets callback functions.
    The callback handlers themselves are stored in ./menubar_callbacks.py
    """
    def __init__(self, parent: QMainWindow):
        """
        Constructs the menu bar with all entries and sets event handlers to be executed when the entry is selected.
        :param parent:
        """
        super(MainMenu, self).__init__(parent)

        # submenu entries and their callbacks
        # (the main entries are below)

        # -- File
        _quit_action = QAction('&Quit', self)
        _quit_action.setShortcut('Ctrl+Q')
        _quit_action.setStatusTip('Exit the application')
        _quit_action.triggered.connect(parent.closeEvent)

        _open_action = QAction('&Open file', self)
        _open_action.setStatusTip('Load data from file')
        _open_action.triggered.connect(parent.open_file)

        _save_action = QAction('&Save', self)
        _save_action.setShortcut('Ctrl+S')
        _save_action.setStatusTip('Save data to opened file')
        _save_action.triggered.connect(parent.save)

        _saveas_action = QAction('&Save as ...', self)
        _saveas_action.setStatusTip('Save data to file')
        _saveas_action.triggered.connect(parent.saveas)

        _export_plot_action = QAction('&Export plot', self)
        _export_plot_action.setShortcut('Ctrl+E')
        _export_plot_action.setStatusTip('Export plot to file')
        _export_plot_action.triggered.connect(parent.export_plot)

        _export_to_json_action = QAction('&Export to JSON', self)
        _export_to_json_action.setShortcut('Ctrl+J')
        _export_to_json_action.setStatusTip('Export plot to file')
        _export_to_json_action.triggered.connect(parent.export_to_json)

        # -- Edit
        _edit_set_zpos_action = QAction('&Set heatmap zpos', self)
        _edit_set_zpos_action.setShortcut('Alt+Z')
        _edit_set_zpos_action.setStatusTip('Determine the height illustrated by the heatmap coloring')
        _edit_set_zpos_action.setStatusTip('Determine the height illustrated by the heatmap coloring')
        _edit_set_zpos_action.triggered.connect(parent.heatmap_zpos_dialog)

        _edit_set_factory_size = QAction('&Set factory size', self)
        _edit_set_factory_size.setShortcut('Alt+H')
        _edit_set_factory_size.setStatusTip('Set width, depth and height of the factory hall')
        _edit_set_factory_size.triggered.connect(parent.factorysize_dialog)

        _edit_set_plmodel = QAction('&Set path loss model', self)
        _edit_set_plmodel.setShortcut('Alt+M')
        _edit_set_plmodel.setStatusTip('Select model and set parameters')
        _edit_set_plmodel.triggered.connect(parent.plmodel_dialog)

        _edit_set_snrparams = QAction('&Set SNR parameters', self)
        _edit_set_snrparams.setShortcut('Alt+S')
        _edit_set_snrparams.setStatusTip('Set SNR parameters')
        _edit_set_snrparams.triggered.connect(parent.snrparams_dialog)

        _edit_set_background_image = QAction('&Set background image', self)
        _edit_set_background_image.setShortcut('Alt+B')
        _edit_set_background_image.setStatusTip('Set a background image for the plot')
        _edit_set_background_image.triggered.connect(parent.bgimage_dialog)


        # -- Help
        _help_about_action = QAction('&About', self)
        _help_about_action.triggered.connect(parent.about_dialog)

        # menubar entries and their associations
        _file_menu = self.addMenu('&File')
        _file_menu.addAction(_quit_action)
        _file_menu.addAction(_open_action)
        _file_menu.addAction(_save_action)
        _file_menu.addAction(_saveas_action)
        _file_menu.addSeparator()
        _file_menu.addAction(_export_plot_action)
        _file_menu.addAction(_export_to_json_action)

        _edit_menu = self.addMenu('&Edit')
        _edit_menu.addAction(_edit_set_zpos_action)
        _edit_menu.addAction(_edit_set_factory_size)
        _edit_menu.addAction(_edit_set_plmodel)
        _edit_menu.addAction(_edit_set_snrparams)
        _edit_menu.addAction(_edit_set_background_image)

        _help_menu = self.addMenu('&Help')
        _help_menu.addAction(_help_about_action)
