# -----------------------------------------------------------
# contains all code related to the toolbar: Layout and methods to change the current tool
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

from PyQt6.QtWidgets import *
import fgfins as fg
from PyQt6.QtGui import QIcon
from PyQt6 import QtCore


class ToolBar(QToolBar):
    """
    Contains all code related to the toolbar: Layout and methods to change the current tool
    """
    def __init__(self, window, *args, **kwargs):
        """
        Creates the toolbar buttons, associates the buttons to the tools and sets the toggle callback
        :param window: Reference to the parent window which contains the currently active tool for any class to read.
        :param args: Additional arguments passed on to QToolBar
        :param kwargs: Additional keyword arguments passed on to QToolBar
        """
        super(ToolBar, self).__init__(*args, **kwargs)
        self.window = window
        self.tbGroup1 = QButtonGroup(self)  # All tool buttons are grouped together so they toggle between each other.

        class ToolbarButton(QToolButton):
            """
            Convenience class to define the buttons. Reduces code repetition
            """
            def __init__(self, parent: QToolBar, icon: QIcon, onToggle, tooltip: str = None,
                         group: QButtonGroup = None, toolId: fg.Tool = None, checked: bool = False):
                super(QToolButton, self).__init__(parent)
                self.setIcon(icon)
                self.setCheckable(True)
                self.setChecked(checked)
                self.toggled.connect(onToggle)
                if tooltip is not None:
                    self.setToolTip(tooltip)
                if toolId is not None:
                    self.toolId = toolId
                if group is not None:
                    group.addButton(self)
                parent.addWidget(self)

        # tool bar buttons are defined here using the class from above
        self.mouse_tb = ToolbarButton(self, QIcon('assets/mouse.svg'), self.toggle_tool,
                                  "Selection Tool", self.tbGroup1, fg.Tool.MOUSE)
        self.addRd_tb = ToolbarButton(self, QIcon('assets/addRadioDot.svg'), self.toggle_tool,
                                  "Add Radio Dots", self.tbGroup1, fg.Tool.ADD_RD)
        self.addUe_tb = ToolbarButton(self, QIcon('assets/addUserEndpoint.svg'), self.toggle_tool,
                                  "Add User Endpoints", self.tbGroup1, fg.Tool.ADD_UE)
        self.addWall_tb = ToolbarButton(self, QIcon('assets/addWall.svg'), self.toggle_tool,
                                        "Add Wall", self.tbGroup1, fg.Tool.ADD_WALL)
        self.rmAc_tb = ToolbarButton(self, QIcon('assets/removeActor.svg'), self.toggle_tool,
                                 "Remove actors", self.tbGroup1, fg.Tool.RM_AC)
        self.addSeparator()  # space between the tool buttons and the legend button
        self.legend_tb = ToolbarButton(self, QIcon('assets/toggleLegend.svg'), self.toggle_legend,
                                   "Show/Hide plot information", checked=True)  # legend button not part of the group

    def toggle_tool(self, checked):
        """
        Callback function that is called twice when the selected tool is changed. In the first call, the old tool is
        deselected, which unselects all actors. In the second call, the new tool is selected.
        Sets the correct event handlers for MatPlotView, to ensure that the plot responds correctly when it is clicked
        on. Also initiates the "multiselect" tip in the statusbar when the click event is selected.
        :param checked:
        """
        if checked:  # second call, new tool selected
            # self.window.cur_tool contains the information about the current tool, so MatPlotView can check anytime
            self.window.cur_tool = QtCore.QObject.sender(self).toolId  # update current tool
            self.window.canvas.set_tool_eventhandlers()  # and set MatPlotView event handlers

            # status bar messages
            if self.window.cur_tool == fg.Tool.MOUSE:
                self.window.statusbar.showMessage("Press CTRL for multi-selection - Press DEL to remove selected")
            elif self.window.cur_tool == fg.Tool.ADD_WALL:
                self.window.statusbar.showMessage("Press CTRL to draw horizontally - Press SHIFT to draw vertically")
            else:
                self.window.statusbar.clearMessage()

        else:
            self.window.canvas.unset_tool_eventhandlers()  # first call, old tool deselected.

    def toggle_legend(self, checked: bool) -> None:
        """
        Callback function that is called when the "Toggle legend" button is toggled.
        :param checked: True if button was activated, false if deactivated
        """
        self.window.canvas.set_labels(checked)  # Instructs MatPlotView to set / remove the labels and legend

    def isLegendTurnedOn(self) -> bool:
        return self.legend_tb.isChecked()
