# -----------------------------------------------------------
# this code manages the data storage where all actors and the factory dimensions are stored
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

from __future__ import annotations
import fgfins as fg
import fgfins.datagenerator as fgdg
from PyQt6 import QtCore
from PyQt6.QtCore import Qt
from inspect import signature
from itertools import repeat
import typing


class ActorModel(QtCore.QAbstractListModel):
    """
    The class of the data storage. It is instantiated only once in the view.py and associated to MatPlotView and the
    SidevarViews as their data model. Whenever data changes are performed in the GUI (e.g. adding an actor, changing
    actor properties in the edit fields), the ActorModel has to be updated. And whenever the ActorModel gets updated
    (meaning data changed), all the views' dataChanged() callback functions are called and have to update their
    visualization in order to be synced with the stored data.
    """
    factory_size_changed = QtCore.pyqtSignal(bool)  # a custom signal for the event that the factory size was changed
    # if bool == true, data within the factory changed as well (we can save one redraw!)

    plmodel_changed = QtCore.pyqtSignal()    # a custom signal for the event that the path loss model was changed
    snrparams_changed = QtCore.pyqtSignal()  # a custom signal for the event that the snr parameters were changed

    def __init__(self, init=None, factory_init=None, pl_model=None, *args, **kwargs):
        """
        Initializes the data storage.
        :param init: A list of actors that should be in the data storage from the start.
        :param factory_init: To set an initial factory different from the default (50x50x10 meters)
        :param args: additional positional arguments passed on to QAbstractListModel
        :param kwargs: additional keyword arguments passed on to QAbstractListModel
        """
        super(ActorModel, self).__init__(*args, **kwargs)
        # the list of items. This is the actual place where all the actors are stored
        self._itemList = init or []
        # the factory. Default size: 50 x 50 x 10 meters
        self.factory = factory_init or fg.Factory(50, 50, 10)
        # the path loss model.
        self._plmodel = pl_model or fgdg.FSPLModel(3.7 * 1e9)
        # the noise and power applied to SNR calculation [in dBm]
        self.noise = -100
        self.power = 29.5

        # when the model is serialized and saved into a file, the software version number is stored too, in order to
        # compare and determine whether a given model (from a file) is compatible with the software version that it's
        # loaded into
        self.version = fg.__version__

        # Flag to determine whether signals should be emitted. Can be turned off temporarily to avoid conflicts
        self.emit = True

    def assign(self, other: ActorModel):
        """
        Take all parameters from the other ActorModel and copy them into this one. This is the same as e.g.
        an overloaded =-operator in C++
        :param other: ActorModel to copy from
        """
        self._itemList = other._itemList
        self.factory = other.factory
        self.emit = False  # to not send an emit when the plmodel is set
        self.plmodel = other.plmodel
        self.emit = other.emit
        self.noise = other.noise
        self.power = other.power

        if self.emit:
            self.dataChanged.emit(self.index(0), self.index(self.rowCount() - 1), [])  # all data changed
            self.factory_size_changed.emit(True)

    def __getstate__(self):
        """
        serialization function to allow transforming an instance of the class to a pickle
        """
        return [item.__getstate__() for item in self._itemList], self.factory.__getstate__(), \
            self.plmodel.__getstate__(), self.emit, self.version, self.noise, self.power

    def __setstate__(self, state):
        """
        deserialization function to allow transforming an instance of the class to a pickle
        """
        super(ActorModel, self).__init__()
        self._itemList = []
        # unroll all the items stored in the list retrieved from pickle
        for item in state[0]:
            numParams = len(signature(item[0]).parameters)
            actor = item[0](*repeat(0, numParams))
            actor.__setstate__(item)  # call setstate from ue or (below) rd respectively
            self._itemList.append(actor)

        self.factory = fg.Factory(50, 50, 10)  # first create a default factory
        self.factory.__setstate__(state[1])  # then override it with the loaded one
        self.plmodel = state[2][0](3.7 * 1e9)
        self.plmodel.__setstate__(state[2])

        self.emit = state[3]
        self.version = state[4]
        self.noise = state[5]
        self.power = state[6]

    def data(self, index: QtCore.QModelIndex, role=Qt.ItemDataRole.DisplayRole):
        """
        Called whenever a view wants to retrieve the actor data responding to a given index. This is a two-step process
        which usually looks as follows:
        selection = self.selectionModel().selection().indexes()  # get the indexes of all selected items
        actor = selection[0].data({role}) # retrieve the data of the first selected item, given a role
        :param index: Index of the data to be retrieved
        :param role: Role of the view. Depending on the role, the data will be formatted differently. This way, a view
        already receives the data in a format which is meaningful for it and doesn't contain unneccessary information.
        See https://doc.qt.io/qt-5/qt.html#ItemDataRole-enum for a list of roles predefined by Qt and
        __init__.py => class Roles(IntEnum) for custom roles that are used within this software
        :return: data as Actor object or list
        """
        # decode the index. The index is in this case just the position of the data in the _itemList array. Simple as
        # that.
        # So why the complicated index system you ask?
        # 1) Because it's just the way it's handled it Qt
        # 2) Say we later want to save the data in a database instead. By using the index system, fn other code outside
        #    the model has to be changed.
        item = self._itemList[index.row()]

        # depending on the role, return the required data in a different format.
        # The MatPlotView just gets the whole Actor/Wall object.
        if role == Qt.ItemDataRole.DisplayRole:
            return item
        # The SidebarDeviceView receives a list with the type of the actor, its id and position
        # In case of a wall, it gets the wall properties
        if role == fg.Roles.DeviceRole:
            if isinstance(item, fg.Actor):
                return [type(item), item.getId(), item.xpos, item.ypos, item.zpos]
            elif isinstance(item, fg.Wall):
                return [type(item), item.startx, item.starty, item.endx, item.endy, item.height, item.material,
                        item.thickness]
            else:
                return []
        # The SidebarQosView receives only UEs. If the requested data is a UE it receives a list with the position and
        # required Path Loss
        if role == fg.Roles.QoSRole:
            if isinstance(item, fg.UserEndpoint):
                return [item.xpos, item.ypos, item.zpos, item.reqPathloss, item.achvPathloss]
            else:
                return []

    def set_factory_size(self, x: float, y: float, z: float):
        """
        Changes the factory size to the given values. If any actor or wall is out of the new (lower) factory bounds,
        it is removed.
        :param x: new width (x)
        :param y: new length (y)
        :param z: new height (z)
        """
        # remove actors/walls that are too out of the factory range
        mark_to_remove = []
        for row in range(0, self.rowCount()):  # iterate over all actors in the data storage
            index = self.index(row)  # convert row to index
            item = index.data()  # convert index to data
            if isinstance(item, fg.Actor):
                if item.xpos > x or item.ypos > y or item.zpos > z:  # if any dimension is outside the new size
                    # the item is marked to remove. It's not removed immediately as this would change the index
                    # references in the middle of the procedure
                    mark_to_remove.append(row)
            if isinstance(item, fg.Wall):
                if item.startx > x or item.endx > x or item.starty > y or item.endy > y:
                    mark_to_remove.append(row)
                elif item.height > z:
                    # walls that are too high should get shrinked to the actual height of the factory
                    item.height = z

        # items are removed from the highest to the lowest to not raise conflicts with index references
        mark_to_remove.sort(reverse=True)
        for row in mark_to_remove:
            self.removeItem(self.index(row), emit=False)

        # and finally, the factory object itself is resized
        self.factory.x = x
        self.factory.y = y
        self.factory.z = z

        # and emit the signal for the views to update their visuals
        self.factory_size_changed.emit(bool(mark_to_remove))  # Argument == true if data changed too

        if mark_to_remove:
            self.dataChanged.emit(self.index(mark_to_remove[0]), self.index(mark_to_remove[-1]), [])

    def get_factory(self) -> fg.Factory:
        return self.factory

    @property
    def plmodel(self):
        return self._plmodel

    @plmodel.setter
    def plmodel(self, plmodel):
        self._plmodel = plmodel
        self.recalculate_all_achvPL()
        if hasattr(self, 'emit') and self.emit:
            self.plmodel_changed.emit()

    @property
    def noise(self):
        return self._noise

    @noise.setter
    def noise(self, noise):
        if (noise > -101) and (noise < 121):
            self._noise = noise
        else:
            raise ValueError('Model: noise out of range: {}'.format(noise))

        if hasattr(self, 'emit') and self.emit:
            self.snrparams_changed.emit()

    @property
    def power(self):
        return self._power

    @power.setter
    def power(self, power):
        if (power > -101) and (power < 121):
            self._power = power
        else:
            raise ValueError('Model: power out of range. Must be between -100 and +120.')

        if hasattr(self, 'emit') and self.emit:
            self.snrparams_changed.emit()

    def setData(self, index: QtCore.QModelIndex, value: typing.Any, role: int = ..., just_a_ue=False) -> bool:
        """
        Replaces any existing stored actor by the given new one.
        Recalculates all UE achvPLs.
        :param index: index of the actor to be replaced
        :param value: new actor to replace the old one with
        :param role: not used
        :param just_a_ue: indicates that the inserted piece of data is a single User Entity
        :return: True if successful, False if there is fn actor associated to this index
        """
        if index.row() < self.rowCount():  # if there is an actor to replace in the storage
            self._itemList[index.row()] = value  # replace it
        else:
            return False

        self.recalculate_all_achvPL()  # as they will change when items change positions

        if self.emit:
            # emit dataChanged signal for the views to update their visuals
            if just_a_ue:
                # signalize that only a UE was added which means the heatmap doesn't need recalculation
                self.dataChanged.emit(index, index, [fg.Roles.AddedSingleUE])
            else:
                self.dataChanged.emit(index, index, [])
        return True

    def recalculate_all_achvPL(self):
        """
        Performs a recalculation of all achieved path loss values of the User Entities and stores the updated UEs.
        """
        # query all radio dots
        radio_dot_indexes = self.indexOfType(fg.RadioDot)
        rd_list = list(map(self.data, radio_dot_indexes))

        # query all walls
        wall_indexes = self.indexOfType(fg.Wall)
        wall_list = list(map(self.data, wall_indexes))

        ue_indexes = self.indexOfType(fg.UserEndpoint)
        for index in ue_indexes:
            ue = index.data()
            new_achvpathloss = fgdg.determine_pathloss(self.plmodel, rd_list, ue.xpos, ue.ypos, ue.zpos, wall_list)
            # noinspection PyPep8Naming
            self._itemList[index.row()].achvPathloss = new_achvpathloss

    def insertRows(self, row: int, count: int, parent: QtCore.QModelIndex = ...) -> bool:
        """
        Allocates storage space for multiple items in the storage. The space is left empty and ready to fill.
        As we are inheriting from QAbstractListModel, this method has to exist.
        It is called by QAbstractListModel's insertRow() to allocate space for a single actor.
        :param row: list index before which the empty rows should be inserted
        :param count: number of empty rows to be inserted
        :param parent: not used
        :return: True when successful
        """
        self.beginInsertRows(QtCore.QModelIndex(), row, row + count - 1)
        self._itemList = self._itemList[:row] + [None] * count + self._itemList[row:]
        self.endInsertRows()
        self.rowsInserted.emit(QtCore.QModelIndex(), row, row + count - 1)  # emit signal, but it's not currently used
        return True

    def removeRows(self, row: int, count: int, parent: QtCore.QModelIndex = ...) -> bool:
        """
        Deletes a consecutive block of rows (and their contents) from the data storage
        As we are inheriting from QAbstractListModel, this method has to exist, but it's not in use currently.
        :param row: list index of the first row to be removed
        :param count: number of consecutive rows to be removed
        :param parent: not used
        :return: True when successful
        """
        self.beginRemoveRows(QtCore.QModelIndex(), row, row + count - 1)
        self._itemList = self._itemList[:row] + self._itemList[row + count:]
        self.endRemoveRows()
        if self.emit:
            self.dataChanged.emit(self.index(row), self.index(row + count - 1), [])  # for views to update visuals
        self.rowsRemoved.emit(QtCore.QModelIndex(), row, row + count - 1)  # emitted but not currently used
        return True

    def rowCount(self, parent=None, *args, **kwargs):
        """
        Returns the number of rows (items + empty rows) currently in the storage.
        As insertRows() is currently only used to allocate space which is then immediately filled with actors, we can
        assume that it returns the number of actors.
        As we are inheriting from QAbstractListModel, this method has to exist.
        :param parent: not used
        :param args: not used
        :param kwargs: not used
        :return: number of rows in the storage
        """
        return len(self._itemList)

    def addItem(self, item: fg.Item):
        """
        Allocate space for a single item in the storage and fill it with the given actor/wall. Emits dataChanged signal.
        The dataChanged signal is also emitted indirectly by insertRows
        :param item: actor to store
        :param just_a_ue: signalized that the
        :return:
        """
        just_a_ue = False
        if isinstance(item, fg.UserEndpoint):
            just_a_ue = True

        # allocate a single new space
        self.insertRow(self.rowCount())  # QAbstractItemView::insertRow calls the above implemented insertRows()
        index = self.index(self.rowCount() - 1, 0, QtCore.QModelIndex())  # get the index of the new space

        # fill space with actor and emit dataChanged signal
        self.setData(index, item, Qt.ItemDataRole.DisplayRole, just_a_ue=just_a_ue)
        pers_index = QtCore.QPersistentModelIndex(index)  # this is pretty much fake, could also return index
        return pers_index

    def addItems(self, itemList: list):
        """
        Allocate space for several actors in the storage and store them. Emits dataChanged signal once.
        :param itemList: list of actors to add
        """
        self.emit = False  # prevent to emit signal multiple times
        for actor in itemList[:-1]:
            self.addItem(actor)  # add the actors one by one, withhold the last
        self.emit = True  # turn signal emitting back on
        self.addItem(itemList.pop(-1))  # and add last actor, emit signal

    def removeItem(self, index: QtCore.QModelIndex, emit=True):
        """
        remove an item (Actor/wall) from the model given its index
        :param index: index of the item to remove
        :param emit: indicates whether the data change should be emitted
        :return: True when successful
        """
        self.emit = emit  # prevent to emit signal multiple times
        self.removeRow(index.row())
        self.emit = True
        return True

    def removeItems(self, indexes: list[QtCore.QModelIndex]):
        """
        remove items (Actor/wall) from the model given their indexes
        :param indexes: list of indexes of the items to remove
        :return: True when successful
        """
        self.emit = False  # prevent to emit signal multiple times
        # first convert indexes to row numbers
        mark_to_remove = []
        for index in indexes:
            mark_to_remove.append(index.row())

        # remove highest rows first, to not mix up the row-indexes of lower rows
        mark_to_remove.sort(reverse=True)

        for row in mark_to_remove:
            self.removeItem(self.index(row))

        self.emit = True
        return True

    def persistentIndexList(self) -> typing.List[QtCore.QModelIndex]:
        """
        Returns a list of all indexes associated to currently stored actors. The index list is meant to be "persistent"
        which means the link between an actor and its index stays the same even if data is freely deleted or added.
        But actually, this is not ensured, as it is currently not a neccessary feature.
        :return: List of indexes
        """
        return [self.index(row) for row in range(0, self.rowCount())]

    def indexOfType(self, itemtype: type) -> list[QtCore.QModelIndex]:
        """
        Returns a list of all indexes associated to currently stored actors of a certain type (either RD, UE or Wall)
        :param itemtype: fn.UserEndpoint, fn.RadioDot or fn.Wall
        :return: list of indexes
        """
        return [self.index(row) for row, item in enumerate(self._itemList) if isinstance(item, itemtype)]
