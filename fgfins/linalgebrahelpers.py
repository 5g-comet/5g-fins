# -----------------------------------------------------------
# Some linear algebra helper functions to determine intersection between walls and line of sight
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

import numpy as np
from typing import Tuple, Union


def get_intersect(la: np.ndarray, lb: np.ndarray, p0: np.ndarray, p1: np.ndarray, p2: np.ndarray)\
        -> Union[Tuple[np.ndarray, bool], bool]:
    """
    Returns the point of intersection between the line defined by points a0, a1 and the plane defined by b0, b1, b2, if
    it exists.
    If fn singular intersection point exists (parallel lines or line embedded in plane), returns False.
    If the point of intersection exists, also determines whether this point is on the line segment between a0 and a1 and
    whether it's on the parallelogram (plane section) defined by b0, b1 and b2.
    Check wikipedia for an explanation of the procedure: https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection
    :param la: (x, y, z) a point on the line
    :param lb: (x, y, z) another point on the line
    :param p0: (x, y, z) a point on the plane
    :param p1: (x, y, z) another point on the plane
    :param p2: (x, y, z) a third point on the plane, which should not be on a line with b0 and b1.
    :return: case 1: singular intersection point exists: tuple((x, y, z), Boolean) - intersection point (x, y, z),
    boolean indicator if on line segment and plane segment
    case 2: fn singular intersection point exists: False
    """

    # check shapes
    if not la.shape == lb.shape == p0.shape == p1.shape == p2.shape == (3,):
        raise ValueError("Incorrect shape of input vectors. Should be compatible to one-dimensional numpy arrays with "
                         "three rows and one column.")

    lab = lb - la
    p01 = p1 - p0
    p02 = p2 - p0

    # solve linear equation system, Solution: (t, u, v). Have to transpose arrays to column vectors
    a = np.column_stack([-lab, p01, p02])
    b = np.array([(la-p0)]).T
    solution = np.linalg.solve(a, b)

    # if all solution parameters are between 0 and 1, the point is on the line segment and plane segment
    on_segments = np.all(np.logical_and(0 < solution, solution < 1))

    # if and only if the determinant is unequal to 0, a unique solution exists (i.e. there is a singular intersection
    # point)
    determinant = np.linalg.det((-lab, p01, p02))

    if determinant:
        return la + lab * solution[2], on_segments  # intersection point = la + lab * t
    else:
        return False  # fn intersection point
