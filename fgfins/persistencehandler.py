# -----------------------------------------------------------
# contains methods to load and store the model data from / to a file
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

from fgfins.model import ActorModel
import fgfins as fgdg
from typing import Optional
from pathlib import Path
from packaging import version
import os.path
import pickle
import json


def load_data(loadpath: str = None) -> Optional[ActorModel]:
    """
    Tries to load the model from a file and deserialize it.
    If fn path is specified: Tries to load data from the standard location %USER-HOME%/.5g-netmonitor/autosave.pickle
    :param loadpath: os path to file location
    :return: ActorModel if successful, None otherwise
    """
    if loadpath is None:
        # specify standard location as path
        loadpath = str(Path.home().joinpath('.5g-netmonitor').joinpath('autosave.pickle'))

    if not os.path.isfile(loadpath):
        return None

    inp = open(loadpath, 'rb')  # open stream as read-binary
    try:
        model = pickle.load(inp)  # deserialize data with pickle
    except EOFError:
        raise ImportError('File is empty or not a pickle')
    inp.close()

    if isinstance(model, ActorModel):
        # check if version of loaded data is compatible with current software version
        # alter this condition to define compatible data versions
        if version.parse(model.version) >= version.parse(fgdg.__compatible_ver__):
            return model
        else:
            # file is from an old, incompatible software version
            raise ImportError('File belongs to an older, incompatible software version')
    else:
        raise ImportError('File is not of the right format')


def save_data(data: ActorModel, savepath: str):
    """
    Serializes and saves the given ActorModel in the location given by savepath. If it doesn't work for some reason,
    the thrown exception is not handled here, but forwarded to the calling function.
    :param data: model to save
    :param savepath: path to location to save to
    """
    outp = open(savepath, 'wb')  # open stream as write-binary
    # save with latest protocol of pickle, to support serialization of latest Python features
    pickle.dump(data, outp, protocol=pickle.HIGHEST_PROTOCOL)
    outp.close()


def save_as_json(data: ActorModel, savepath: str):
    """
    Serializes and saves the given ActorModel in human-readable JSON in the location given by savepath. If it doesn't
    work for some reason, the thrown exception is not handled here, but forwarded to the calling function.
    The JSON is meant to provide a machine-readable format for other software to process the data. It is not meant to be
    re-imported into the software.
    :param data: model to save
    :param savepath: path to location to save to
    """
    outpj = open(savepath, 'w')
    jstr = json.dumps(data, default=lambda o: {"type": getattr(type(o), '__name__')} | getattr(o, '__dict__', str(o)),
                      indent=4)
    outpj.write(jstr)
    outpj.close()
