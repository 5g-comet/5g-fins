# -----------------------------------------------------------
# 5G NETWORK MONITOR - software to plot and analyze 5G wireless network scnenarios with respect to their quality
# using a statistical pathloss_matrix model
#
# This file: a wrapper to start the application. Initiates the creation of a window as defined in fgfins/view.py
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

import fgfins.view as nov
from PyQt6.QtWidgets import QApplication
import sys

if __name__ == "__main__":
    def run():
        app = QApplication(sys.argv)
        _ = nov.Window()
        sys.exit(app.exec())

    run()
