# -----------------------------------------------------------
# a test script with examples that illustrate how to use and test the path loss models without the GUI software
#
# (C) 2022 RWTH Aachen University, ISEK Research Group -- https://isek.rwth-aachen.de
# part of the 5G-COMET project: https://www.ipt.fraunhofer.de/de/projekte/5gcomet.html
# project representative at ISEK: Kiraseya Preusser -- kiraseya.preusser*at*isek.rwth-aachen.de
# Developer: Marcel Garling -- marcel.garling*at*rwth-aachen.de
# Developer: Phillip Lölver -- phillip.loelver*at*rwth-aachen.de
# -----------------------------------------------------------

import fgfins.datagenerator as nodg

if __name__ == "__main__":
    print("This is a test environment to verify the results of the path-loss models")
    frequency = 3.7 * 1e9  # 1 GHz == 1e9 Hz
    distance = 1000  # meters

    # 1: FSPL, 2: InF-SH
    PL_Model = 1

    if PL_Model == 1:
        print("FSPL")
        print("--------------------------------------------------")

        # First, let's create an FSPL model with the given frequency
        fspl = nodg.FSPLModel(frequency)

        # now we calculate the pathloss [dB] at the given distance
        pathloss = fspl.calculate_pathloss(distance)
        print("Pathloss: " + str(pathloss))

        print()
        print("Environment setup")
        print("--------------------------------------------------")

        # Okay now let's simulate a real environment. In a room with 50 meters length and height, we'd like to have
        # 3 Radio Dots at random position, in a height of 5 meters.
        rd_list = nodg.create_rds_at_random(3, 300, 300, 5)

        # What is the position of the first radio dot?
        first_rd = rd_list[0]
        print("First RD x-pos: " + str(first_rd.xpos))
        print("First RD y-pos: " + str(first_rd.ypos))

        # Given this environment, let's calculate the whole path-loss matrix (1 value for each meter/pixel of the heatmap.
        matrix = nodg.calculate_pathloss_matrix(fspl, rd_list, 300, 300, 5)
        # print('\n'.join([''.join(['{:4}'.format(pathloss) for pathloss in row]) for row in matrix]))
        pass
        # of course it's easier and more beautiful to just set a breakpoint and inspect the data with the debugger instead

    elif PL_Model == 2:
        print("InF-SH Environment setup")
        print("--------------------------------------------------")

        three_gpp = nodg.InFSHPLModel(frequency)

        # Simulation of a factory environment. Room with 50 meters length and width
        # 3 radiodots at random position, at a height of 5 meters
        rd_list = nodg.create_rds_at_random(3, 50, 50, 5)

        # Calculation of whole path-loss matrix at height of 3 meters (1 value for each meter/pixel of the heatmap)
        matrix = nodg.calculate_pathloss_matrix(three_gpp, rd_list, 50, 50, 3)
        pass
        # Set breakpoint on pass and view as array to inspect heatmap




