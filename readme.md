# 5G-FINS
This software allows for visualization and analysis of a 5G network using a statistical model.

![](doc/screenshot.png)

## Installation
In order to ensure a defined environment for the code to run in, the project was created using Anaconda. We recommend to follow this approach and install Anaconda on the user's machine as well. Anaconda takes care of the right versioning of Python and any dependencies.

After installing Anaconda, create a new environment based on the information contained in [environment.yml](environment.yml).
(Documentation on that [here](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file))
(The [environment_all.yml](environment_all.yml) file exists for completeness reasons, as it contains ALL versions of ALL modules on which the code was tested. It is not neccessarily portable, thus use this one only if the regular one results in errors or undesired behaviour.)

## Top-level structure
- `assets/` - icons
- `doc/` - contains assets for the documentation. The actual documentation consists of the readme files in the different directories and the entries in the project wiki on GitLab.
- `fgfins/` - the actual software code as a package
- `start.py` - executable entry point that starts the app
- `test.py` - file that shows exemplarily how the classes of the software can be used to perform model calculations without a GUI, just inside a python script.
